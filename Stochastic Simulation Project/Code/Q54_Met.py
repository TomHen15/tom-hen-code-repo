"""
Question 54 - Hybrid Hamiltonian Sampler (Algo 3)
"""

import matplotlib
import acor as acor
import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
import numdifftools as nd
from scipy.stats import multivariate_normal as MVN
from time import gmtime, mktime

#################################################
######## Main Functions used by sampler #########

def img_plot1(a,b,N,h):
    x_vals = np.arccos(np.cos(a))
    y_vals = np.arccos(np.cos(b))
    fig = plt.figure()
    ax = fig.add_subplot(111)
    H, xedges, yedges = np.histogram2d(y_vals, x_vals)
    im = matplotlib.image.NonUniformImage(ax, interpolation='bilinear')
    xcenters = xedges[:-1] + 0.5 * (xedges[1:] - xedges[:-1])
    ycenters = yedges[:-1] + 0.5 * (yedges[1:] - yedges[:-1])
    im.set_data(xcenters, ycenters, H)
    ax.images.append(im)
    ax.set_xlim(xedges[0], xedges[-1])
    ax.set_ylim(yedges[0], yedges[-1])
    ax.set_aspect('equal')
    plt.title("XY model Metropolized Hybrid Sampler .\n N = " +str(N)+ " h = " + str(h))
    plt.show()


def boltzman_dens(H,x_hat,x_tilde):
    """ Proportional to Boltzman density with kernel function H
        The input variables are:
        H = kernel function
        x = evaluation point
    Returns:
        evaluates exp(-H(x))
    """
    return np.exp(-H(x_hat,x_tilde))


def accProb_boltzman(H,x_hat,x_tilde,y_hat,y_tilde):
    """ computes acceptance probability for algorithm 3 based on boltzman density
    The input variables are:
        H = kernel function
        X = current step
        Y = proposed step
    Returns:
        acceptance probability
    """
    numer = boltzman_dens(H,y_hat,y_tilde)
    denom = boltzman_dens(H,x_hat,x_tilde)
    if denom == 0:
        return (0,0)
    else:
        return (min(1,numer/denom),numer/denom)

        
def ODE_evolution(n,L,y_hat,y_tilde,A,h,gradLogpi):
    """ evolves th ODE based on formulas on page 103
    The input variables are:
        n = number of evolution steps
        L = dimension of problem (length of lattice in XY model)
        y_tilde, y_hat = initialization points
        A = A_hat matrix from page 103
        h = step size parameter
        gradLogpi = function handle to evaluate gradient of logpi
    Returns:
       y_tilde and y_hat after n steps of evolution
    """
    y_tilde_vals = [y_tilde]
    y_hat_vals = [y_hat]
    
    for i in range(n):
        y_hat = y_hat_vals[i]
        y_tilde= y_tilde_vals[i]
        y_hat_arr = np.squeeze(y_hat)                                               #for gradLogpi computation
        gradLogpi_y_hat = np.transpose(np.matrix(gradLogpi(y_hat_arr)))             #evaluate gradLogpi at y_hat_i       
        y_tilde_prime = np.transpose(np.matrix(y_tilde)) + h/2*A*gradLogpi_y_hat    
        gradK_y_tilde_prime = y_tilde_prime                                         #for our K the gradient is identity
        y_hat_new = np.transpose(np.matrix(y_hat)) + h*A*gradK_y_tilde_prime
        y_hat_new = np.squeeze(np.array(y_hat_new))                                 #for gradLogpi computation
        gradLogpi_y_hat_new = np.transpose(np.matrix(gradLogpi(y_hat_new)))         #evaluate gradLogpi at y_hat_new     
        y_tilde_new = y_tilde_prime+h/2*np.transpose(A)*gradLogpi_y_hat_new
        y_tilde_new = np.squeeze(np.array(y_tilde_new))
        if L == 1: #this handles output type for case where L=1 (sloppy)
            y_tilde_new = np.array([float(y_tilde_new)])
            y_hat_new = np.array([float(y_hat_new)])
        y_tilde_vals.append(y_tilde_new)
        y_hat_vals.append(y_hat_new)
        
    return (y_hat_vals[-1],y_tilde_vals[-1])  


def sim_step_hybrid(n,L,x_hat,A,H,h,gradLogpi):
    """ computes the proposal Y based on step (2) of algorithm (3)
    The input variables are:
        n = number of evolution steps for ODE
        L = dimension of problem (length of lattice in XY model)
        x_hat = initialization point
        A = A_hat matrix from page 103
        H = kernel function for Boltzman density
        h = step size parameter
        gradLogpi = function handle to evaluate gradient of logpi
    Returns:
       proposal step vector
    """
    # draw random sample for density proportional to exp(-K(x))
    mean = [0]*L
    cov = np.eye(L)
    if L == 1:
        y_tilde = np.array([MVN.rvs(mean,cov,1)])
    else:
        y_tilde = MVN.rvs(mean,cov,1)
    # get Y^(k) as solution to (5.14) by evolving the ODE:
    y_new = ODE_evolution(n,L,x_hat,y_tilde,A,h,gradLogpi)
    x_hat_new = y_new[0] #this is the actual sample
    y_tilde_new = y_new[1]
    # the first output is the sample
    # the second output is conjugate variable
    # the third output is the previous conjugate variable
    return (x_hat_new,y_tilde_new,y_tilde)

  
def sim_step_hybrid_met(n,L,x_hat,A,H,h,gradLogpi):
    """ computes the proposal Y based on step (2) of algorithm (3)
        then computes acceptance probability and updates accordingly
    The input variables are:
        n = number of evolution steps for ODE
        L = dimension of problem (length of lattice in XY model)
        x_hat = initialization point
        A = A_hat matrix from page 103
        H = kernel function for Boltzman density
        h = step size parameter
        gradLogpi = function handle to evaluate gradient of logpi
    Returns:
       next step in chain
    """
    temp = sim_step_hybrid(n,L,x_hat,A,H,h,gradLogpi)
    y_hat_new = temp[0]   #first component is the new y_hat 
    y_tilde_new = temp[1] #second component is the new y_tilde 
    y_tilde_old = temp[2] #third componnet is the old y_tilde 
    # compute the acceptance probability
    acc_prob = accProb_boltzman(H,x_hat,y_tilde_old,y_hat_new,y_tilde_new)
    acc_prob = acc_prob[0]
    # decide whether to stay or move
    decision = np.random.binomial(1,acc_prob,1)
    # print(acc_prob,decision)
    if decision==1:
        return (y_hat_new,acc_prob,int(decision))
    else:
        return (x_hat,acc_prob,int(decision))    

        
def SampleXY_hybrid_met_Ntimes(N,L,n,X,A,H,h,gradLogpi):
    """ performs N steps of algorithm (3) with metropolis step
    The input variables are:
        N = number of steps to take in the chain
        L = dimension of problem (length of lattice in XY model)
        n = number of ODE evolutions
        X = starting point
        A = A_hat matrix from page 103
        H = kernel function for Boltzman density
        h = step size parameter
        gradLogpi = function handle to evaluate gradient of logpi
    Returns:
        N steps of the markov chain for XY model
    """
    acc_probs = []
    decisions = []
    sample = [X] 
    for i in range(N):
        x_hat = sample[-1] #last entry in sample          
        temp = sim_step_hybrid_met(n,L,x_hat,A,H,h,gradLogpi) # perform the metropolis step
        x_hat_new = temp[0] # this is the new x_hat           
        acc_prob = temp[1] # the acceptance prob of the last decision
        decision = temp[2] # the decision value of the last decision
        sample.append(x_hat_new)
        acc_probs.append(acc_prob)
        decisions.append(decision)
    return (sample, acc_probs, decisions)

    
#################################################    
#### Additional Functions for IAT computation ###

def compSigma(angles,L):
    """Converts matrix of angles to list of matrices of XY vectors of norm 1
    Input Variables:
        angles = output of sampler
        L = lenght of lattice
    Output:
       A list of arrays containing the XY vectors corresponding to the angles
    """
    sigmas = []
    for i in range(L):
        temp = [np.array([float(np.cos(angle)), float(np.sin(angle))]) for angle in angles[:,i]] 
        temp = np.array(temp)
        sigmas.append(temp)
    return sigmas
  
    
def compMagnetization(angles,L): 
    """ Computes the angle of magnetization from a list of angles
    Input Variables:
        angles = output of sampler
        L = lenght of lattice
    Output:
        Vector of cosine of angles of magnetization 
    """
    sigmas = compSigma(angles,L)
    x_values = []
    y_values = []   
    for i in range(L):
        temp = sigmas[i]
        x_values.append(temp[:,0])
        y_values.append(temp[:,1])
    
    x_values = np.vstack(x_values).T
    y_values = np.vstack(y_values).T
    M_x = x_values.sum(axis=1)
    M_y = y_values.sum(axis=1)
    res = M_x/np.sqrt(M_x**2+M_y**2)
    return res    
  
    
def acorComp(angles,L):
    """ Computes IAT of cosine of angle of magnetization
        Input Variables:
        angles = output of sampler
        L = lenght of lattice
    Output:
        IAT for cosine of angle of magnetization 
    """
    M = compMagnetization(angles,L)
    tau, mean, sigma = acor.acor(M)
    return tau     


############### For Plotting #####################
### plots of 2d-heatmap and marginal histogram ###

def dostuff(samples,i,k):
    """ samples = list of samples
        i = number of sample (0 to 4)
        k = length to take from sample 
    """
    h_vals = [0.01, 0.05, 0.1, 0.5, 1]
    h = h_vals[i]
    S = samples[i]
    S = S[0:k,:]
    a = S[:,0] #first angle in each sample
    b = S[:,1] #second angle in each sample   
    # plot 2-dimensional heatmap
    img_plot1(a,b,k,h) 
    loc = "/Users/Seraph/Dropbox/University/PhD/Year 2/Stochastic Simulation/Images/Q54_Met/"
    name = "2Dplot_"+str(k)+"_"+str(h)
    plt.savefig(loc+name+".jpg")
    plt.close("all")
    # plot histogram of first angle
    bins = np.linspace(0,np.pi,25)
    c = a % (np.pi)
    c = np.array(c).flatten()
    c = c.tolist()
    hist = plt.hist(c, bins=bins)
    plt.title("Histogram of First Angle.\n N = " +str(k)+ " h = " + str(h))
    name2 = "AngleHIst_"+str(k)+"_"+str(h)
    plt.savefig(loc+name2+".jpg")
    plt.close("all")
    #print("the IAT is ", acorComp(S,L)) 

    
N_vals = [500, 1000, 5000, 10000]
for i in range(5):
    for j in range(4):
        k = N_vals[j]
        dostuff(samples,i,k)    
   
       
## plot IAT as function of log(h)
logh_vals = np.log(h_vals)
plt.close("all")
for i in range(4):
    n = N_vals[i]
    plt.plot(logh_vals,IATs[:,i],label='N='+str(n))
plt.title("IAT as function of h parameter (Metropolized)")
plt.legend(loc='upper right')
plt.xlabel("log(h)")
plt.ylabel("IAT")
plt.show()
loc = "/Users/Seraph/Dropbox/University/PhD/Year 2/Stochastic Simulation/Images/Q54_Met/"
name = "Q54_Met_IAT"
plt.savefig(loc+name+".jpg")


#################################################
#################################################
if __name__ == "__main__":
    # initialization:
    L = 2                                                #length of lattice
    C = np.eye(L)                                        # The C matrix 
    K = lambda x: 0.5*(np.linalg.norm(x,2)**2)           # K function for Hamiltonian
    H = lambda x_hat,x_tilde: -logpi(x_hat)+K(x_tilde)   # Hamiltonian Function
    n = 5                                                # number of ODE evolutions
    A = np.eye(L)                                        # The A matrix   
    x_hat = np.random.uniform(0,2*np.pi,size=L)          # starting point

    # density and related functions
    pi = lambda theta: np.exp(sum(np.cos(theta[1:L]-theta[0:-1]))+np.cos(theta[L-1]-theta[0])) 
    logpi = lambda theta: sum(np.cos(theta[1:L]-theta[0:-1]))+np.cos(theta[L-1]-theta[0]) 
    gradLogpi = nd.Gradient(logpi)

    # Values of h and N to use
    h_vals = [0.01, 0.05, 0.1, 0.5, 1]
    N_vals = [500,1000, 5000, 10000]
    
    IATs = np.zeros((len(h_vals), len(N_vals)))     #holds IAT values
    Accepts = np.zeros((len(h_vals), len(N_vals)))  #holds number of accepted moves
    RTs = np.zeros((len(h_vals), len(N_vals)))      #holds compute times 
    samples = []                                    #this holds the actual samples
    
    for i in range(len(h_vals)): 
        N = 10000 #we only compute the 10000 length sample for each h value
        h = h_vals[i]
        stime = gmtime() 
        S = SampleXY_hybrid_met_Ntimes(N,L,n,x_hat,A,H,h,gradLogpi)
        RT = (mktime(gmtime()) - mktime(stime))
        print("finished simulation with h= ",h,"in time ", RT)
        accept = S[2]   #metropolis decisions
        sample = S[0]   #sampled angles
        sample = sample[1:(N+1)]
        sample = np.vstack(sample)
        samples.append(sample)
        for j in range(len(N_vals)):
            n = N_vals[j]
            temp = sample[0:n,:]
            IAT = acorComp(temp,L)
            RT2 = RT*n/float(N)                                                #relative RT for sample of given size
            IATs[i,j] = IAT                                             #save IAT 
            RTs[i,j] = RT2                                              #save runtime                    
            Accepts[i,j] = np.count_nonzero(accept[0:n])/float(n)*100   #save accepted number of moves
                       
            
## save results to file:
loc = "/Users/Seraph/Dropbox/University/PhD/Year 2/Stochastic Simulation/Data/Q54/"
name = "Q54_Met"
np.save(loc+name,np.array(samples))
name2 = "Q54_Met_IAT"
np.save(loc+name2,IATs)
name3 = "Q54_Met_Accepts"
np.save(loc+name3,Accepts)
name3 = "Q54_Met_RTs"
np.save(loc+name3,RTs)