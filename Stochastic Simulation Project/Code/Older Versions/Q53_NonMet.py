"""
Question 53 - Non Metropolized sampler
"""

import matplotlib
import acor as acor
import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
import numdifftools as nd
from scipy.stats import multivariate_normal as MVN
from time import gmtime, mktime

#################################################
def img_plot1(a,b,N,h):
    x_vals = np.arccos(np.cos(a))
    y_vals = np.arccos(np.cos(b))
    x_vals = x_vals.flatten().tolist()[0]
    y_vals = y_vals.flatten().tolist()[0]

    fig = plt.figure()
    ax = fig.add_subplot(111)
    H, xedges, yedges = np.histogram2d(y_vals, x_vals)
    im = matplotlib.image.NonUniformImage(ax, interpolation='bilinear')
    xcenters = xedges[:-1] + 0.5 * (xedges[1:] - xedges[:-1])
    ycenters = yedges[:-1] + 0.5 * (yedges[1:] - yedges[:-1])
    im.set_data(xcenters, ycenters, H)
    ax.images.append(im)
    ax.set_xlim(xedges[0], xedges[-1])
    ax.set_ylim(yedges[0], yedges[-1])
    ax.set_aspect('equal')
    plt.title("XY model Non-Metropolized Sampler .\n N = " +str(N)+ " h = " + str(h))
    plt.show()

def sim_step(L,X,C,h,gradLogpi):
    """ One step of MCMC based on formula 5.6
    The input variables are:
        L = size of lattice
        X = previous step in simulated chain (or starting point)
            this should be a row vector of L angles
        C = positive definite matrix
        h = step size parameter
        gradLogpi = function handle to evaluate gradient of logpi
    Returns:
        next step in chain as a row vector
    """
    Xarr = np.squeeze(np.asarray(X)) #gradient computation expect array not vector
    gradVec = np.transpose(np.matrix(gradLogpi(Xarr))) # Column gradient vector
    sqrtC = sp.linalg.sqrtm(C)
    xi = np.transpose(np.matrix(2*np.random.binomial(1,0.5,L)-1))
    X_new = np.transpose(np.matrix(X)) + h*C*gradVec +np.sqrt(2*h)*sqrtC*xi
    return np.transpose(X_new)


def SampleXY_Ntimes(N,L,X,C,h,gradLogpi):
    """ performs N steps of MCMC simulation based on formula 5.6
    The input variables are:
        N = number of steps to take in the chain
        L = dimension of problem (length of lattice in XY model)
        X = starting point, this should be a row vector of L angles
        C = positive definite matrix
        h = step size parameter
        gradLogpi = function handle to evaluate gradient of logpi
    Returns:
        N steps of the markov chain for XY model
    """
    sample = [X]
    for i in range(N):
        prevx = sample[-1]
        nextx = sim_step(L,prevx,C,h,gradLogpi)
        sample.append(nextx)
    return sample



#################################################
## Additional Functions for IAT computation ##
def compSigma(angles,L):
    """Converts matrix of angles to list of matrices of XY vectors of norm 1
    Input Variables:
        angles = output of sampler
        L = lenght of lattice
    Output:
       A list of arrays containing the XY vectors corresponding to the angles
    """
    sigmas = []
    for i in range(L):
        temp = [np.array([float(np.cos(angle)), float(np.sin(angle))]) for angle in angles[:,i]]
        temp = np.array(temp)
        sigmas.append(temp)
    return sigmas


def compMagnetization(angles,L):
    """ Computes the angle of magnetization from a list of angles
    Input Variables:
        angles = output of sampler
        L = lenght of lattice
    Output:
        Vector of cosine of angles of magnetization
    """
    sigmas = compSigma(angles,L)
    x_values = []
    y_values = []
    for i in range(L):
        temp = sigmas[i]
        x_values.append(temp[:,0])
        y_values.append(temp[:,1])

    x_values = np.vstack(x_values).T
    y_values = np.vstack(y_values).T
    M_x = x_values.sum(axis=1)
    M_y = y_values.sum(axis=1)
    res = M_x/np.sqrt(M_x**2+M_y**2)
    return res


def acorComp(angles,L):
    """ Computes IAT of cosine of angle of magnetization
        Input Variables:
        angles = output of sampler
        L = lenght of lattice
    Output:
        IAT for cosine of angle of magnetization
    """
    M = compMagnetization(angles,L)
    tau, mean, sigma = acor.acor(M)
    return tau


#################################################
############## Test for XY Model ##############
L = 2 #lattice size
logpi = lambda theta: sum(np.cos(theta[1:L]-theta[0:-1]))+np.cos(theta[L-1]-theta[0])
gradLogpi = nd.Gradient(logpi)

C = np.eye(L)
h = 0.01
X = np.asmatrix(np.random.uniform(0,2*np.pi,size=L)) #starting angles

N = 50000 #number of samples
t1 = gmtime()
S = SampleXY_Ntimes(N,L,X,C,h,gradLogpi)
print("run time", (mktime(gmtime()) - mktime(t1)))
sample = np.vstack(S)
a = sample[:,0] #first angle in each sample
b = sample[:,1] #second angle in each sample

acorComp(sample,L)  #compute IAT for cosine of angle of magnetization

# plot 2-dimensional heatmap
img_plot1(a,b,N,h)

# plot histogram of first angle
bins = np.linspace(0,2*np.pi,50)
c = a % (2*np.pi)
c = c.tolist()[0]
hist = plt.hist(c, bins=bins)
plt.title("Histogram of First Angle.\n N = " +str(N)+ " h = " + str(h))
plt.show(hist)