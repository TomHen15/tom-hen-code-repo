""" 
Question 59, Isotonic Metropolis Sampler 
"""

import matplotlib
import acor as acor
import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
import numdifftools as nd
from scipy.stats import multivariate_normal as MVN
from time import gmtime, mktime

#################################################   
def img_plot2(sample,N,h):
    """ sim is a tuple that holds the results of the MCMC 
        auxillary function that plots 2d histograms of the first two angles.
    """
    xvals = sample[:,0].flatten().tolist()[0]
    yvals = sample[:,1].flatten().tolist()[0] 
    fig = plt.figure()
    ax = fig.add_subplot(111)
    xedges = [x/10.0 for x in range(-30, 30)]
    yedges = [x/10.0 for x in range(-30, 30)]   
    H, xedges, yedges = np.histogram2d(yvals, xvals, bins = (xedges, yedges))
    im = matplotlib.image.NonUniformImage(ax, interpolation='bilinear')
    xcenters = xedges[:-1] + 0.5 * (xedges[1:] - xedges[:-1])
    ycenters = yedges[:-1] + 0.5 * (yedges[1:] - yedges[:-1])
    im.set_data(xcenters, ycenters, H)
    ax.images.append(im)
    ax.set_xlim(xedges[0], xedges[-1])
    ax.set_ylim(yedges[0], yedges[-1])
    ax.set_aspect('equal')
    plt.title("Rosenbrock Isotonic Metropolis Sampler.\n N = " +str(N)+ " h = " + str(h))
    plt.ylabel("$Y$") 
    plt.xlabel("$X$") 

def sim_step_isotonic(d,X,C,h,gradLogpi):
    """ One step of MCMC based on formula 5.6 with standard normal xi variables
    The input variables are:
        d = dimension of problem
        X = previous step in simulated chain (or starting point)
        C = positive definite matrix
        h = step size parameter
        gradLogpi = function handle to evaluate gradient of logpi
    Returns:
        next step in chain as a row vector
    """
    Xarr = np.squeeze(np.asarray(X)) #gradient computation expect array not vector
    gradVec = np.transpose(np.matrix(gradLogpi(Xarr))) # Column gradient vector
    sqrtC = sp.linalg.sqrtm(C)
    xi = np.transpose(np.matrix(np.random.normal(0,1,d)))
    X_new = np.transpose(np.matrix(X)) + h*C*gradVec +np.sqrt(2*h)*sqrtC*xi
    return np.transpose(X_new)

    
def transDensity(X,Y,C,h,gradLogpi):
    """ computes function proportion to transition density
        where the transition uses standard normal random variables
        this is based on the formula in page 106 of the notes
    The input variables are:       
        X = previous step in simulated chain (or starting point)
        Y = next step in simulated chain with MVN transition variables
        C = positive definite matrix
        h = step size parameter
        gradLogpi = function handle to evaluate gradient of logpi
    Returns:
        next step in chain as a row vector
    """
    Xarr = np.squeeze(np.asarray(X))
    gradVec = np.transpose(np.matrix(gradLogpi(Xarr)))
    c1 = Y-X-h*np.transpose(C*gradVec) #row vector
    c2 = (np.transpose(Y-X)-h*C*gradVec) #column vector
    Cinv = np.linalg.inv(C)
    return np.exp(-1/(4*h)*c1*Cinv*c2)
    
    
def accProb(X,Y,C,h,pi,gradLogpi):
    """ computes the transition probability 
    The input variables are:       
        X = previous step in simulated chain (or starting point)
        Y = next step in simulated chain with MVN transition variables
        C = positive definite matrix
        h = step size parameter
        pi = function handle proportional to the target density
        gradLogpi = function handle to evaluate gradient of logpi
    returns:
        pacc by formula on page 106 of notes
    """
    TDensXY = transDensity(X,Y,C,h,gradLogpi) 
    TDensYX = transDensity(Y,X,C,h,gradLogpi)
    Xarr = np.squeeze(np.asarray(X))
    Yarr = np.squeeze(np.asarray(Y))
    numer = TDensYX*pi(Yarr)
    denom = TDensXY*pi(Xarr)   
    if denom == 0:
        return (0,0)
    else:
        return (min(1,numer/denom),numer/denom)
   
    
def Sample_Isotonic_Ntimes(N,d,X,C,h,pi,gradLogpi):
    """ performs N steps of MCMC simulation based on formula 5.6
        this version includes the metropolis decision step
    The input variables are:
        N = number of steps to take in the chain
        d = dimension of the problem
        X = starting point, this should be a row vector of L angles
        C = positive definite matrix
        h = step size parameter
        pi = function handle proportional to the target density
        gradLogpi = function handle to evaluate gradient of logpi
    Returns:
        N steps of the markov chain for XY model
    """
    acc_probs = []
    decisions = []
    sample = [X]
    for i in range(N):
        prevx = sample[-1]
        propx = sim_step_isotonic(d,prevx,C,h,gradLogpi) #proposal step
        acc_prob = accProb(prevx,propx,C,h,pi,gradLogpi) #accept probability
        acc_prob = acc_prob[0]
        decision = np.random.binomial(1,acc_prob,1) #decision whether to accept or reject
        if decision==1:
            nextx = propx
        else:
            nextx = prevx
            
        sample.append(nextx)
        acc_probs.append(acc_prob)
        decisions.append(decision)
    return (sample, acc_probs, decisions)
    
    
def acorComp(sample):
    """" Computes IAT for x and y coordinate of sample
    Input Variables:
        sample = the sample
    Output:
        list with 2 entries containing IAT for x-coord and IAT for y-coord
    """
    xvals = sample[:,:,0].flatten()
    yvals = sample[:,:,1].flatten()
    IATx,b,c = acor.acor(xvals)
    IATy,b,c = acor.acor(yvals)
    return [IATx,IATy]
    
    
############## Rosenbrock Simulation ##############
pi = lambda x: np.exp((-1.0/20)*(100*((x[1]-x[0]**2)**2)+((1-x[0])**2)))
logpi = lambda x: (-1.0/20)*(100*((x[1]-x[0]**2)**2)+((1-x[0])**2))  
gradLogpi = nd.Gradient(logpi)
    
d = 2 #dimension of problem
C = 1/20*np.eye(d)
h = 0.1 
X = np.random.normal(0,1,d) 
X = np.array(X) #starting point

N = 20000
t1 = gmtime() 
S = Sample_Isotonic_Ntimes(N,d,X,C,h,pi,gradLogpi)
print("run time", (mktime(gmtime()) - mktime(t1)))
sample = S[0]
sample = sample[1:(N+1),:]
sample = np.vstack(sample)

img_plot2(sample,N,h)
np.mean(sample[:,0])
np.mean(sample[:,1])
acorComp(sample)

#################################################
#################################################
if __name__ == "__main__":
    # initialization:
    d = 2                                #dimension of problem
    C = 1/20*np.eye(d)                   #The C matrix                             
    X = np.random.normal(0,1,d) 
    X = np.array(X)                      #starting point

    # density and related functions
    pi = lambda x: np.exp((-1.0/20)*(100*((x[1]-x[0]**2)**2)+((1-x[0])**2)))
    logpi = lambda x: (-1.0/20)*(100*((x[1]-x[0]**2)**2)+((1-x[0])**2))  
    gradLogpi = nd.Gradient(logpi)

    # Values of h and N to use
    h_vals = [0.01, 0.05, 0.1, 0.5, 1]
    N_vals = [1000, 5000, 10000, 20000, 50000]
    
    IATs = np.zeros((len(h_vals), len(N_vals)))     #holds IAT values
    Accepts = np.zeros((len(h_vals), len(N_vals)))  #holds number of accepted moves
    RTs = np.zeros((len(h_vals), len(N_vals)))      #holds compute times 
    samples = []                                    #this holds the actual samples
    
    for i in range(len(h_vals)): 
        N = 50000 #we only compute the 50000 length sample for each h value
        h = h_vals[i]
        stime = gmtime() 
        S = Sample_Isotonic_Ntimes(N,d,X,C,h,pi,gradLogpi)
        RT = (mktime(gmtime()) - mktime(stime))
        print("finished simulation with h= ",h,"in time ", RT)
        accept = S[2]   #metropolis decisions
        sample = S[0]   #the actual sample
        sample = sample[1:(N+1)]
        sample = np.vstack(sample)
        samples.append(sample)
        for j in range(len(N_vals)):
            n = N_vals[j]
            temp = sample[0:n,:]
            IAT = acorComp(temp)
            RT2 = RT*n/N                                 #relative RT for sample of given size
            IATs[i,j] = IAT                              #save IAT 
            RTs[i,j] = RT2                               #save runtime                    
            Accepts[i,j] = np.count_nonzero(accept[0:n]) #save accepted number of moves
    
## save results to file:
loc = "/Users/Seraph/Dropbox/University/PhD/Year 2/Stochastic Simulation/Data/"
name = "Q59_Isotonic"
np.save(loc+name,np.array(samples))
