"""
Question 59, Ensemble based sampler
"""

import matplotlib
import acor as acor
import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
import copy as copy
import numdifftools as nd
from scipy.stats import multivariate_normal as MVN
from time import gmtime, mktime

#################################################
def img_plot2(xvals,yvals,N,L,a):
    """ sim is a tuple that holds the results of the MCMC 
        auxillary function that plots 2d histograms of the first two angles.
    """
    fig = plt.figure()
    ax = fig.add_subplot(111)
    xedges = [x/10.0 for x in range(-30, 30)]
    yedges = [x/10.0 for x in range(-30, 30)]   
    H, xedges, yedges = np.histogram2d(yvals, xvals, bins = (xedges, yedges))
    im = matplotlib.image.NonUniformImage(ax, interpolation='bilinear')
    xcenters = xedges[:-1] + 0.5 * (xedges[1:] - xedges[:-1])
    ycenters = yedges[:-1] + 0.5 * (yedges[1:] - yedges[:-1])
    im.set_data(xcenters, ycenters, H)
    ax.images.append(im)
    ax.set_xlim(xedges[0], xedges[-1])
    ax.set_ylim(yedges[0], yedges[-1])
    ax.set_aspect('equal')
    plt.title("Rosenbrock Ensemble Sampler.\n N = " +str(N)+ " a = " + str(a) + " L = " + str(L))
    plt.ylabel("$Y$") 
    plt.xlabel("$X$") 
    

def g_dens(a,z):
    """ computes the g density as function of parameter a and value z in (0,inf)"""
    if (float(z) in np.array([1/float(a),float(a)])):
        ind = 1
    else:
        ind = 0
    out = 1/float(np.sqrt(float(z)))*ind
    return out
    
    
def draw_from_g(a):
    """ returns either a or 1/a with probabilities according to g"""
    normconst = 1/np.sqrt(float(a))+np.sqrt(float(a)) #normalizing constant for g
    p1 = 1/(np.sqrt(float(a))*normconst) #probability to return a
    #p2 = np.sqrt(float(a))/normconst # probability to return 1/a
    ind = np.random.binomial(1,p1,1)
    if ind==1:
        res = a
    else:
        res = 1/float(a)
    return res 
    
    
def accProb2(X,Y,Z,d,pi):
    """ computes the accept probability based on formula 6.11
    The input variables are:  
        X = previous step
        Y = proposal step
        Z = random draw from the density g
        d = dimension of the problem
        pi = function handle proportional to the target density
    returns:
        pacc based on formula 6.
    """
    temp = float(Z)**(d-1)*(float(pi(Y))/float(pi(X)))
    res = (min(1,temp),temp)
    return res
    
    
def prop_walker(Xi,Xj,Z,a):
    """ returns a proposal move for the i'th walker
    The input variables are:  
        Xi = current position of walker i 
        Xj = current position of walker j
        Z = random draw from the density g
        a = parameter for the independent distribution g
    returns:
        Y = proposal step
    """
    Z = draw_from_g(a) 
    Y = Xj+Z*(Xi-Xj)
    return Y
    
    
def one_step_for_walker(X,i,pi,a,d,L):
    """ performs the MC update for the i'th walker
    The input variables are:  
        X = array of walker vectors
        i = index to operate on
        pi = function handle proportional to the target density
        a = parameter for the independent distribution g
        d = dimension of problem
        L = number of walkers
    returns:
        next step of the walker
    """
    # select inedx j different than i
    j=i
    while j==i:
        j = np.random.randint(0, high=L, size=1) 
        j = j[0]
    # create proposal for walker Xi  
    Z = draw_from_g(a) 
    Y = X[j]+Z*(X[i]-X[j])
    acc_prob = accProb2(X[i],Y,Z,d,pi)
    acc_prob = acc_prob[0]
    # make decision whether to accept or reject and update accordingly
    decision = np.random.binomial(1,acc_prob,1) 
    if decision==1:
        newxi = Y
    else:
        newxi = X[i]
    # return the entire chain after updating walker i
    return (newxi,j,Z,acc_prob,decision[0])

    
def one_step_for_chain(X,pi,a,d,L):
    """ makes one step of the MC for the entire chain
    The input variables are:  
        X = array of walker vectors at current step
        pi = function handle proportional to the target density
        a = parameter for the independent distribution g
        d = dimension of problem
        L = number of walkers
    returns:
        next step of the chain
    """ 
    nextx = copy.copy(X)
    for i in range(L):
        T = one_step_for_walker(X,i,pi,a,d,L)
        nextx[i,:] = T[0]
    return nextx 
  
      
def N_steps_for_chain(N,X,pi,a,d,L):
    """ makes N steps of the MC for the entire chain
    The input variables are:  
        N = number of steps
        X = list of walker vectors at initial step
        pi = function handle proportional to the target density
        a = parameter for the independent distribution g
        d = dimension of problem
        L = number of walkers
    returns:
        list of N lists of L walker vectors
    """ 
    sample = []
    for i in range(N):
        if len(sample)==0:
            prevx = copy.copy(X)
        else:
            prevx = copy.copy(sample[-1])
        nextx = one_step_for_chain(prevx,pi,a,d,L)
        sample.append(nextx)
    return sample       
      

def acorComp(sample):
    """" Computes IAT for x and y coordinate of sample
    Input Variables:
        sample = the sample
    Output:
        list with 2 entries containing IAT for x-coord and IAT for y-coord
    """
    xvals = sample[:,:,0].flatten()
    yvals = sample[:,:,1].flatten()
    IATx,b,c = acor.acor(xvals)
    IATy,b,c = acor.acor(yvals)
    return [IATx,IATy]
    
    
############## Rosenbrock Simulation ##############
L = 100 #number of walkers
a = 2 #parameter for g_density

pi = lambda x: np.exp((-1.0/20)*(100*((x[1]-x[0]**2)**2)+((1-x[0])**2)))
d = 2 #dimension of problem    
X = [np.random.normal(0,1,2) for i in range(L)]
X = np.array(X) #starting points for walkers

N = 1000
t1 = gmtime() 
S = N_steps_for_chain(N,X,pi,a,d,L)
print("run time", (mktime(gmtime()) - mktime(t1)))
S = np.array(S)
xvals = S[:,:,0].flatten()
yvals = S[:,:,1].flatten()

img_plot2(xvals,yvals,N,L,a)
np.mean(S[:,:,0]) #second n=index is walker, third index is sample coordinate
np.mean(S[:,:,1]) #second n=index is walker, third index is sample coordinate
acorComp(S)


############### For Plotting ####################
def dostuff(samples,i,k): NOT READY
    """ samples = list of samples
        i = number of sample (0 to 4)
        k = length to take from sample 
    """
    h_vals = [0.01, 0.05, 0.1, 0.5, 1]
    h = h_vals[i]
    S = samples[i]
    S = S[0:k,:]
    a = S[:,0] #first angle in each sample
    b = S[:,1] #second angle in each sample   
    # plot 2-dimensional heatmap
    img_plot2(a,b,k,h) 
    loc = "/Users/Seraph/Dropbox/University/PhD/Year 2/Stochastic Simulation/Images/Q59_Ensemble/"
    name = "2Dplot_"+str(k)+"_"+str(h)
    plt.savefig(loc+name+".jpg")
    plt.close("all")
    # plot histogram of first angle
    bins = np.linspace(0,np.pi,25)
    c = a % (np.pi)
    c = np.array(c).flatten()
    c = c.tolist()
    hist = plt.hist(c, bins=bins)
    plt.title("Histogram of First Angle.\n N = " +str(k)+ " h = " + str(h))
    name2 = "AngleHIst_"+str(k)+"_"+str(h)
    plt.savefig(loc+name2+".jpg")
    plt.close("all")
    #print("the IAT is ", acorComp(S,L)) 

    
N_vals = [1000, 5000, 10000, 20000, 50000]
for i in range(5):
    for j in range(5):
        k = N_vals[j]
        dostuff(samples,i,k)    
  

#################################################        
#################################################
if __name__ == "__main__":
    # initialization:
    d = 2  #dimension of problem

    # density and related functions
    pi = lambda x: np.exp((-1.0/20)*(100*((x[1]-x[0]**2)**2)+((1-x[0])**2)))

    # Values of L and N to use
    L_vals = [10,100,500,1000]         #number of walkers
    a_Vals = [2,4,8]                   #the a parameter 
    N_vals = [100,1000,10000]          #size of sample
    
    IATs_x = np.zeros((len(L_vals), len(N_vals)))   #holds IAT values for x coordinate
    IATs_y = np.zeros((len(L_vals), len(N_vals)))   #holds IAT values for y coordinate
    RTs = np.zeros((len(L_vals), len(N_vals)))      #holds compute times 
    samples = []                                    #this holds the actual samples
    
    for i in range(len(L_vals)): 
        N = 10000  #we only compute the 10000 length sample for each L value
        L = L_vals[i]
        X = [np.random.normal(0,1,2) for i in range(L)]
        X = np.array(X) #starting points for walkers
        stime = gmtime() 
        S = N_steps_for_chain(N,X,pi,a,d,L)
        RT = (mktime(gmtime()) - mktime(stime))
        print("finished simulation with L= ",L,"and a= ",a,"in time", RT)
        sample = np.array(S)   #sample
        samples.append(sample)
        for j in range(len(N_vals)):
            n = N_vals[j]
            temp = sample[0:n,:,:]
            IAT = acorComp(temp)
            RT2 = RT*n/N                                 #relative RT for sample of given size
            IATs_x[i,j] = IAT[0]                          #save IAT for x-coord
            IATs_y[i,j] = IAT[0]                          #save IAT for y-coord
            RTs[i,j] = RT2                               #save runtime                    

## save results to file:
loc = "/Users/Seraph/Dropbox/University/PhD/Year 2/Stochastic Simulation/Data/"
name = "Q59_Ensemble"
np.save(loc+name,np.array(samples))
