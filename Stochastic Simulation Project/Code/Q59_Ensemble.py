"""
Question 59, Ensemble based sampler
"""

import matplotlib
import acor as acor
import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
import copy as copy
import numdifftools as nd
from scipy.stats import multivariate_normal as MVN
from time import gmtime, mktime

#################################################
######## Main Functions used by sampler #########

def img_plot2(sample,N,L,a):
    """ Plots 2d histograms of the first two angles """
    xvals = sample[:,:,0].flatten().tolist()
    yvals = sample[:,:,1].flatten().tolist()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    xedges = [x/10.0 for x in range(-30, 30)]
    yedges = [x/10.0 for x in range(-30, 30)]
    H, xedges, yedges = np.histogram2d(yvals, xvals, bins = (xedges, yedges))
    im = matplotlib.image.NonUniformImage(ax, interpolation='bilinear')
    xcenters = xedges[:-1] + 0.5 * (xedges[1:] - xedges[:-1])
    ycenters = yedges[:-1] + 0.5 * (yedges[1:] - yedges[:-1])
    im.set_data(xcenters, ycenters, H)
    ax.images.append(im)
    ax.set_xlim(xedges[0], xedges[-1])
    ax.set_ylim(yedges[0], yedges[-1])
    ax.set_aspect('equal')
    plt.title("Rosenbrock Ensemble Sampler.\n N = " +str(N)+ " L = " + str(L)+ " a = " + str(a))
    plt.ylabel("$Y$")
    plt.xlabel("$X$")


def g_dens(a,z):
    """ computes the g density as function of parameter a and value z in (0,inf)"""
    if (float(z) in np.array([1/float(a),float(a)])):
        ind = 1
    else:
        ind = 0
    out = 1/float(np.sqrt(float(z)))*ind
    return out


def draw_from_g(a):
    """ returns either a or 1/a with probabilities according to g"""
    normconst = 1/np.sqrt(float(a))+np.sqrt(float(a)) #normalizing constant for g
    p1 = 1/(np.sqrt(float(a))*normconst) #probability to return a
    #p2 = np.sqrt(float(a))/normconst # probability to return 1/a
    ind = np.random.binomial(1,p1,1)
    if ind==1:
        res = a
    else:
        res = 1/float(a)
    return res


def accProb2(X,Y,Z,d,pi):
    """ computes the accept probability based on formula 6.11
    The input variables are:
        X = previous step
        Y = proposal step
        Z = random draw from the density g
        d = dimension of the problem
        pi = function handle proportional to the target density
    returns:
        pacc based on formula 6.
    """
    if float(pi(X))==0:
        return (0,0)
    else:
        temp = float(Z)**(d-1)*(float(pi(Y))/float(pi(X)))
        if temp == 0:
            return (0,0)
        else:
            return (min(1,temp),temp)


def prop_walker(Xi,Xj,Z,a):
    """ returns a proposal move for the i'th walker
    The input variables are:
        Xi = current position of walker i
        Xj = current position of walker j
        Z = random draw from the density g
        a = parameter for the independent distribution g
    returns:
        Y = proposal step
    """
    Z = draw_from_g(a)
    Y = Xj+Z*(Xi-Xj)
    return Y


def one_step_for_walker(X,i,pi,a,d,L):
    """ performs the MC update for the i'th walker
    The input variables are:
        X = array of walker vectors
        i = index to operate on
        pi = function handle proportional to the target density
        a = parameter for the independent distribution g
        d = dimension of problem
        L = number of walkers
    returns:
        next step of the walker
    """
    # select inedx j different than i
    j=i
    while j==i:
        j = np.random.randint(0, high=L, size=1)
        j = j[0]
    # create proposal for walker Xi
    Z = draw_from_g(a)
    Y = X[j]+Z*(X[i]-X[j])
    acc_prob = accProb2(X[i],Y,Z,d,pi)
    acc_prob = acc_prob[0]
    # make decision whether to accept or reject and update accordingly
    decision = np.random.binomial(1,acc_prob,1)
    if decision==1:
        newxi = Y
    else:
        newxi = X[i]
    # return the entire chain after updating walker i
    return (newxi,j,Z,acc_prob,decision[0])


def one_step_for_chain(X,pi,a,d,L):
    """ makes one step of the MC for the entire chain
    The input variables are:
        X = array of walker vectors at current step
        pi = function handle proportional to the target density
        a = parameter for the independent distribution g
        d = dimension of problem
        L = number of walkers
    returns:
        next step of the chain
        number of accepted moves in iteration
    """
    nextx = copy.copy(X)
    acc_count = 0
    for i in range(L):
        T = one_step_for_walker(X,i,pi,a,d,L)
        nextx[i,:] = T[0]
        if T[4]==1:
            acc_count = acc_count+1
    return (nextx,acc_count)


def N_steps_for_chain(N,X,pi,a,d,L):
    """ makes N steps of the MC for the entire chain
    The input variables are:
        N = number of steps
        X = list of walker vectors at initial step
        pi = function handle proportional to the target density
        a = parameter for the independent distribution g
        d = dimension of problem
        L = number of walkers
    returns:
        list of N lists of L walker vectors
        list of counts of accepted moves
    """
    sample = []
    acc_counts = []
    for i in range(N):
        if len(sample)==0:
            prevx = copy.copy(X)
        else:
            prevx = copy.copy(sample[-1])
        temp = one_step_for_chain(prevx,pi,a,d,L)
        nextx = temp[0]
        acc_num= temp[1]
        sample.append(nextx)
        acc_counts.append(acc_num)
    return (sample,acc_counts)


#################################################
#### Additional functions for IAT computation ###

def acorComp_AllWalkers(sample):
    """" Computes IAT for x and y coordinate of sample
    Input Variables:
        sample = the sample
    Output:
        list with 2 entries containing IAT for x-coord and IAT for y-coord
    """
    xvals = sample[:,:,0].flatten()
    yvals = sample[:,:,1].flatten()
    IATx,b,c = acor.acor(xvals)
    IATy,b,c = acor.acor(yvals)
    return [IATx,IATy]


def acorComp_OneWalker(sample,i):
    """" Computes IAT for x and y coordinate of one walker
    Input Variables:
        sample = the sample
        i = index of walker
    Output:
        list with 2 entries containing IAT for x-coord and IAT for y-coord
    """
    xvals = sample[:,i,0].flatten()
    yvals = sample[:,i,1].flatten()
    IATx,b,c = acor.acor(xvals)
    IATy,b,c = acor.acor(yvals)
    return [IATx,IATy]


def acorComp_AveragedWalkers(sample):
    """" Computes IAT for x and y coordinate of average across walkers
    Input Variables:
        sample = the sample
     Output:
        list with 2 entries containing IAT for x-coord and IAT for y-coord
    """
    N = sample.shape[0]
    temp = (1.0/N)*np.sum(sample,axis=1)
    xvals = temp[:,0]
    yvals = temp[:,1]
    IATx,b,c = acor.acor(xvals)
    IATy,b,c = acor.acor(yvals)
    return [IATx,IATy]


############### For Plotting #####################
############ save plots of 2d-heatmap ############

def dostuff(samples,L,a,k):
    """ samples = list of samples
        L,a parameters
        k = length to take from sample
    """
    S = samples[i]
    S = S[0:k,:,:]
    # plot 2-dimensional heatmap
    img_plot2(S,k,L,a)
    loc = "/Users/Seraph/Dropbox/University/PhD/Year 2/Stochastic Simulation/Images/Q59_Ensemble/"
    name = "2Dplot_N"+str(k)+"_L"+str(L)+"_a"+str(a)
    plt.savefig(loc+name+".jpg")
    plt.close("all")


N_vals = [100, 1000, 10000]
for k in N_vals:
    for L in L_vals:
        for a in a_vals:
            samples = samples_dict[a]
            dostuff(samples,L,a,k)


############### More IAT stuff ##################
####### Compute IAT only for first walker #######
IAT0_x = np.zeros((len(L_vals), len(N_vals)))
IAT0_y = np.zeros((len(L_vals), len(N_vals)))
samples = samples_dict[2]
for i in range(len(L_vals)):
    sample = samples[i]
    for j in range(len(N_vals)):
        N = N_vals[j]
        temp = sample[0:N,:,:]
        IAT = acorComp_OneWalker(temp,0)
        IAT0_x[i,j] = IAT[0]
        IAT0_y[i,j] = IAT[1]

####### Compute IAT for averaged walkers ########
IATavg_x = np.zeros((len(L_vals), len(N_vals)))
IATavg_y = np.zeros((len(L_vals), len(N_vals)))
samples = samples_dict[2]
for i in range(len(L_vals)):
    sample = samples[i]
    for j in range(len(N_vals)):
        N = N_vals[j]
        temp = sample[0:N,:,:]
        IAT = acorComp_AveragedWalkers(temp)
        IATavg_x[i,j] = IAT[0]
        IATavg_y[i,j] = IAT[1]


#################################################
######### Compute % of accepted moves ###########
N = 10000
Accepted_percent = np.zeros((len(L_vals), len(a_vals)))
for j in range(len(a_vals)):
    a = a_vals[j]
    rejected_counts = Rej_dict[a]
    rejected_counts = rejected_counts[2,:]
    for i in range(len(L_vals)):
        L = L_vals[i]
        tot_moves = L*N
        Accepted_percent[i,j] = (1.0/tot_moves)*rejected_counts[i]*100


#################################################
#################################################
if __name__ == "__main__":
    # initialization:
    d = 2  #dimension of problem

    # density and related functions
    pi = lambda x: np.exp((-1.0/20)*(100*((x[1]-x[0]**2)**2)+((1-x[0])**2)))

    # Values of L, a and N to use
    a_vals = [2,4,8]                   #the a parameter
    L_vals = [10,100,1000]         #number of walkers
    N_vals = [100,1000,10000]          #size of sample

    # init dictionaries that hold results for different "a" values
    samples_dict = {}
    IATs_x_dict = {}
    IATs_y_dict = {}
    RTs_dict = {}
    acc_dict = {}

    for k in range(len(a_vals)):  # loop over values of the "a" parameter
        a = a_vals[k]
        IATs_x = np.zeros((len(L_vals), len(N_vals)))     #holds IAT values for x coordinate for current "a" value
        IATs_y = np.zeros((len(L_vals), len(N_vals)))     #holds IAT values for y coordinate for current "a" value
        RTs = np.zeros((len(L_vals), len(N_vals)))        #holds compute times for current "a" value
        acc_counts = np.zeros((len(L_vals), len(N_vals))) #holds number of rejections per chain step
        samples = []                                      #holds the actual samples for current "a" value

        for i in range(len(L_vals)): # loop over values of L parameter
            N = 10000  #we only compute the 10000 length sample for each a,L pair
            L = L_vals[i]
            X = [np.random.normal(0,1,2) for s in range(L)]
            X = np.array(X) #starting points for walkers
            stime = gmtime()
            temp = N_steps_for_chain(N,X,pi,a,d,L)
            RT = (mktime(gmtime()) - mktime(stime))
            print("finished simulation with L= ",L,"and a= ",a,"in time", RT)
            S = temp[0]             #the sample
            acc_count = temp[1]     #the acceptance counts
            sample = np.array(S)    #convert sample to array
            samples.append(sample)
            for j in range(len(N_vals)):
                n = N_vals[j]
                temp = sample[0:n,:,:]
                acc_counts[i,j] = sum(acc_count[0:(n+1)])
                IAT = acorComp_AllWalkers(temp)
                RT2 = RT*n/float(N)                      #relative RT for sample of given size
                IATs_x[i,j] = IAT[0]                     #save IAT for x-coord
                IATs_y[i,j] = IAT[1]                     #save IAT for y-coord
                RTs[i,j] = RT2                           #save runtime
        samples_dict[a] = samples                        #put into dictionary
        IATs_x_dict[a] = IATs_x                          #put into dictionary
        IATs_y_dict[a] = IATs_y                          #put into dictionary
        RTs_dict[a] = RTs                                #put into dictionary
        acc_dict[a] = acc_counts                         #put into dictionary

## save results to file:
loc = "/Users/Seraph/Dropbox/University/PhD/Year 2/Stochastic Simulation/Data/Q59/"
## Save the samples
name = "Q59_Ensemble_a2_L10"
samples = samples_dict[2]
sample = samples[0]
np.save(loc+name,sample)
name = "Q59_Ensemble_a2_L100"
samples = samples_dict[2]
sample = samples[1]
np.save(loc+name,sample)
name = "Q59_Ensemble_a2_L1000"
samples = samples_dict[2]
sample = samples[2]
np.save(loc+name,sample)

name = "Q59_Ensemble_a4_L10"
samples = samples_dict[4]
sample = samples[0]
np.save(loc+name,sample)
name = "Q59_Ensemble_a4_L100"
samples = samples_dict[4]
sample = samples[1]
np.save(loc+name,sample)
name = "Q59_Ensemble_a4_L1000"
samples = samples_dict[4]
sample = samples[2]
np.save(loc+name,sample)

name = "Q59_Ensemble_a8_L10"
samples = samples_dict[8]
sample = samples[0]
np.save(loc+name,sample)
name = "Q59_Ensemble_a8_L100"
samples = samples_dict[8]
sample = samples[1]
np.save(loc+name,sample)
name = "Q59_Ensemble_a8_L1000"
samples = samples_dict[8]
sample = samples[2]
np.save(loc+name,sample)

### SAVE IAT DATA ###
name = "Q59_Ensemble_IATx_a2"
IAT = IATs_x_dict[2]
np.save(loc+name,IAT)
name = "Q59_Ensemble_IATx_a4"
IAT = IATs_x_dict[4]
np.save(loc+name,IAT)
name = "Q59_Ensemble_IATx_a8"
IAT = IATs_x_dict[8]
np.save(loc+name,IAT)
name = "Q59_Ensemble_IATy_a2"
IAT = IATs_y_dict[2]
np.save(loc+name,IAT)
name = "Q59_Ensemble_IATy_a4"
IAT = IATs_y_dict[4]
np.save(loc+name,IAT)
name = "Q59_Ensemble_IATy_a8"
IAT = IATs_y_dict[8]
np.save(loc+name,IAT)

### SAVE RT DATA ###
name = "Q59_Ensemble_RT_a2"
RT = RTs_dict[2]
np.save(loc+name,RT)
name = "Q59_Ensemble_RT_a4"
RT = RTs_dict[2]
np.save(loc+name,RT)
name = "Q59_Ensemble_RT_a8"
RT = RTs_dict[2]
np.save(loc+name,RT)

### SAVE accepted % for a=2 ###
name = "Q59_Ensemble_AccPercent_a2"
np.save(loc+name,Accepted_percent)