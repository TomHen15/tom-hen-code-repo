"""
Question 53 - Non Metropolized sampler
"""

import acor as acor
import numpy as np
import scipy as sp
import numdifftools as nd
from time import gmtime, mktime

#################################################
######## Main Functions used by sampler #########

def sim_step(L,X,C,h,gradLogpi):
    """ One step of MCMC based on formula 5.6
    The input variables are:
        L = size of lattice
        X = previous step in simulated chain (or starting point)
            this should be a row vector of L angles
        C = positive definite matrix
        h = step size parameter
        gradLogpi = function handle to evaluate gradient of logpi
    Returns:
        next step in chain as a row vector
    """
    Xarr = np.squeeze(np.asarray(X)) #gradient computation expect array not vector
    gradVec = np.transpose(np.matrix(gradLogpi(Xarr))) # Column gradient vector
    sqrtC = sp.linalg.sqrtm(C)
    xi = np.transpose(np.matrix(2*np.random.binomial(1,0.5,L)-1))
    X_new = np.transpose(np.matrix(X)) + h*C*gradVec +np.sqrt(2*h)*sqrtC*xi
    return np.transpose(X_new)


def SampleXY_Ntimes(N,L,X,C,h,gradLogpi):
    """ performs N steps of MCMC simulation based on formula 5.6
    The input variables are:
        N = number of steps to take in the chain
        L = dimension of problem (length of lattice in XY model)
        X = starting point, this should be a row vector of L angles
        C = positive definite matrix
        h = step size parameter
        gradLogpi = function handle to evaluate gradient of logpi
    Returns:
        N steps of the markov chain for XY model
    """
    sample = [X]
    for i in range(N):
        prevx = sample[-1]
        nextx = sim_step(L,prevx,C,h,gradLogpi)
        sample.append(nextx)
    return sample


#################################################
### Additional Functions for IAT computation ####

def compSigma(angles,L):
    """Converts matrix of angles to list of matrices of XY vectors of norm 1
    Input Variables:
        angles = output of sampler
        L = lenght of lattice
    Output:
       A list of arrays containing the XY vectors corresponding to the angles
    """
    sigmas = []
    for i in range(L):
        temp = [np.array([float(np.cos(angle)), float(np.sin(angle))]) for angle in angles[:,i]]
        temp = np.array(temp)
        sigmas.append(temp)
    return sigmas


def compMagnetization(angles,L):
    """ Computes the angle of magnetization from a list of angles
    Input Variables:
        angles = output of sampler
        L = lenght of lattice
    Output:
        Vector of cosine of angles of magnetization
    """
    sigmas = compSigma(angles,L)
    x_values = []
    y_values = []
    for i in range(L):
        temp = sigmas[i]
        x_values.append(temp[:,0])
        y_values.append(temp[:,1])
    x_values = np.vstack(x_values).T
    y_values = np.vstack(y_values).T
    M_x = x_values.sum(axis=1)
    M_y = y_values.sum(axis=1)
    res = M_x/np.sqrt(M_x**2+M_y**2)
    return res


def acorComp(angles,L):
    """ Computes IAT of cosine of angle of magnetization
        Input Variables:
        angles = output of sampler
        L = lenght of lattice
    Output:
        IAT for cosine of angle of magnetization
    """
    M = compMagnetization(angles,L)
    tau, mean, sigma = acor.acor(M)
    return tau


#################################################
#################################################
if __name__ == "__main__":
    # initialization:
    L = 2                                                #length of lattice
    C = np.eye(L)
    X = np.asmatrix(np.random.uniform(0,2*np.pi,size=L)) #starting point

    # density and related functions
    logpi = lambda theta: sum(np.cos(theta[1:L]-theta[0:-1]))+np.cos(theta[L-1]-theta[0])
    gradLogpi = nd.Gradient(logpi)

    # Values of h and N to use
    h_vals = [0.01, 0.05, 0.1, 0.5, 1]
    N_vals = [1000, 5000, 10000, 20000, 50000]

    IATs = np.zeros((len(h_vals), len(N_vals)))     #holds IAT values
    RTs = np.zeros((len(h_vals), len(N_vals)))      #holds compute times
    samples = []                                    #this holds the actual samples

    for i in range(len(h_vals)):
        N = 50000 #we only compute the 50000 length sample for each h value
        h = h_vals[i]
        stime = gmtime()
        S = SampleXY_Ntimes(N,L,X,C,h,gradLogpi)
        RT = (mktime(gmtime()) - mktime(stime))
        print("finished simulation with h= ",h,"in time ", RT)
        sample = np.vstack(S)
        sample = sample[1:(N+1),:]
        samples.append(sample)
        for j in range(len(N_vals)):
            n = N_vals[j]
            temp = sample[0:n,:]
            IAT = acorComp(temp,L)
            RT2 = RT*n/float(N)                          #relative RT for sample of given size
            IATs[i,j] = IAT                              #store IAT
            RTs[i,j] = RT2                               #store runtime

    # save results to file:
    ## save results to file:
    loc = "/Users/Seraph/Dropbox/University/PhD/Year 2/Stochastic Simulation/Data/Q53/"
    name = "Q53_NonMet"
    np.save(loc+name,np.array(samples))
    name2 = "Q53_NonMet_IAT"
    np.save(loc+name2,IATs)
    name3 = "Q53_NonMet_RTs"
    np.save(loc+name3,RTs)