-----------------------------------------------------------
Python Version: Python 2.7.12 |Anaconda custom (x86_64)| (default, Jul  2 2016, 17:43:17)
-----------------------------------------------------------
Required modules:
- Numpy
- Scipy
- acor (pip install acor)
- ndifftools
-----------------------------------------------------------
Description of code:
- The code folder contains 6 script files whose names indicates which question and algorithm they implement. The scripts do not produce the plots found in the PDF, they only compute and save the outputs that I used in order to produce the results.
- In order to save the results you will need to edit one of the last lines of the code that contains the path in which to save all the data, this line always starts with "loc = "PATH..."", all you need to do is replace PATH with a valid directory.

In the main portion of each of the scripts I have already set all the parameters for that specific algorithm. However, everything is fairly well documented and you should be able to change the initialization parameters if you so wish.

