# README #

This repo contains some examples of coding projects I worked on during my graduate degree. 
The main contribution is the code that I used for the numerical simulations conducted as part of my thesis dissertation.
In addition there are some other examples of smaller projects I worked on during the degree.

tomhen@gmail.com