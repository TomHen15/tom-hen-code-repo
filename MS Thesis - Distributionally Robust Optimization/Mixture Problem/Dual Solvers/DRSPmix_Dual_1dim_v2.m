function out = DRSPmix_Dual_1dim_v2(H,etas,alphas,mus,sigmas,gam1,gam2)
    %% Input:
    % H         = Function handle for optimization target
    % etas      = matrix of mixture component vectors
    % alphas    = vector of mixture coefficients
    % mus       = vector of means for components
    % sigmas    = vector of variances for components
    % gam1      = vector of gamma1 constants
    % gam2      = vector of gamma2 constants
    % gam1 must be >= 0, gam2 must be >= 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if not((min(gam1)>=0)&&(min(gam2)>=1))
        disp('Error: can not solve the problem');
        disp('gam1 must be >= 0, gam2 must be >= 1');
        return
    end

    [~,K] = size(gam1);   % number of mixture components
    
    cvx_begin quiet
        variables x(1) q(K) Q(K) p(K) P(K) r(K) s(K) 
        expressions T1(K) T2(K) T3(K) T4(K) T5(K) Z(2,2,K) Z2(M,K)
        for k = 1:K
            T1(k) = (gam2(k)*sigmas(k) - mus(k)^2) * Q(k);
            T2(k) = sigmas(k) * P(k);
            T3(k) = -2*mus(k) * p(k);
            T4(k) = gam1(k)*s(k);
            T5(k) = p(k) + Q(k)*mus(k);
            Z(:,:,k) = [P(k),p(k);p(k),s(k)];
            Z2(:,k) = alphas(k)*H(x,etas{k}) - etas{k}.^2*Q(k) + 2*T5(k)*etas{k};
        end
        minimize(sum(r+T1+T2+T3+T4))
        subject to
        for k = 1:K
            r(k) >= Z2(:,k);
            Q(k) >= 0;
            Z(:,:,k) == semidefinite(2);
        end 
    cvx_end
    out = [cvx_optval,x];
end