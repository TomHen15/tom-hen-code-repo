function out = DRSPmix_Equivalent_v3(H,etas,alphas,MUS,SIGMAS,gam1,gam2)
%% Input:
% H         = Function handle for optimization target
% etas      = cell array of mixture component data
% alphas    = vector of mixture coefficients
% MUS       = matrix of mean parameters for components
% SIGMAS    = matrix of variance parameters for components
% gam1      = vector of gamma1 constants
% gam2      = vector of gamma2 constants
% gam1 must be >= 0, gam2 must be >= 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This version uses cellfun for the r constraint
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if not((min(gam1)>=0)&&(min(gam2)>=1))
    disp('Error: can not solve the problem');
    disp('gam1 must be >= 0, gam2 must be >= 1');
    return
end

[n,~] = size(MUS);      % dimension of vectors in problem
[~,K] = size(gam1);     % number of mixture components

cvx_begin quiet
    variables x(1,n) q(n,K) r(K) t(K) Q(n,n,K)
    expressions T1(K) T2(K) T3(K)
    for k=1:K
            T1(k) = trace((gam2(k)*SIGMAS(:,:,k) + MUS(:,k)*MUS(:,k)')*Q(:,:,k));
            T2(k) = norm(sqrtm(SIGMAS(:,:,k)) * (q(:,k) + 2*Q(:,:,k)*MUS(:,k)),2);
            T3(k) = sqrt(gam1(k))*T2(k) + MUS(:,k)'*q(:,k);
    end
    minimize(sum(r) + sum(t))
    subject to
        for k = 1:K
            [N,~]    = size(etas{k});
            z1       = (repmat(x,N,1)-etas{k});
            w1       = alphas(k).*norms(z1,2,2);
            w2       = sum(etas{k}*Q(:,:,k).*etas{k},2);
            w3       = (q(:,k)'*etas{k}')';
            r(k)     >= w1-w2-w3;
            t(k)     >= T1(k) + T3(k);
            Q(:,:,k) == semidefinite(n);
        end   
cvx_end
out = {cvx_optval,x};
end