function out = DRSPmix_Equivalent_1dim_v2(H,etas,alphas,mus,sigmas,gam1,gam2)
    %% Input:
    % H         = Function handle for optimization target
    % etas      = cell array of mixture component vectors
    % alphas    = vector of mixture coefficients
    % mus       = vector of means for components
    % sigmas    = vector of variances for components
    % gam1      = vector of gamma1 constants
    % gam2      = vector of gamma2 constants
    % gam1 must be >= 0, gam2 must be >= 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if not((min(gam1)>=0)&&(min(gam2)>=1))
        disp('Error: can not solve the problem');
        disp('gam1 must be >= 0, gam2 must be >= 1');
        return
    end

    [~,K] = size(gam1);   % number of mixture components
    
    cvx_begin quiet
        variables x(1) q(K) Q(K) r(K) t(K) 
        expressions T1(K) T2(K) 
        for k=1:K
            T1(k) = (gam2(k)*sigmas(k) + mus(k)^2)*Q(k) + mus(k)*q(k);;
            T2(k) = sqrt(gam1(k)) * abs(sqrt(sigmas(k))*(q(k) + 2*Q(k)*mus(k))); 
        end 
        minimize(sum(r) + sum(t))
        subject to
        for k = 1:K
            r(k) >= alphas(k)*H(x,etas{k}) - etas{k}.^2*Q(k) - etas{k}*q(k);
            t(k) >= T1(k) + T2(k)
            Q(k) >= 0;
        end 
    cvx_end
    out = [cvx_optval,x];
end