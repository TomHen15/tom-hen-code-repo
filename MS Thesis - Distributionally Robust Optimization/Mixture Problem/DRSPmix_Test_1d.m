%%%% This is for test of equivalence between different mDRSP solvers in one-dimension %%%%
% generate data for two component test
n = 1;                              %dimension of problem
K = 2;                              %number of mixture components
N = 5000;                          %number of xi vectors
alpha  = 0.5;                       %mixture coefficient
alphas = [alpha,1-alpha];
MUS = randn(n,K);
SIGMAS = zeros(n,n,K);
for k=1:K
    SIGMAS(:,:,k) = eye(n);
end

% generate the mixture components
etas = cell(K,1);
for k=1:K
    etas{k} = mvnrnd(MUS(:,k),SIGMAS(:,:,k),N);
end

H = @(x,y) abs(x-y);
gam1 = [1,1]; gam2 = [1,1];

t1_1 = cputime;
out1 = DRSPmix_Equivalent_1dim_v2(H,etas,alphas,MUS,SIGMAS,gam1,gam2);
t2_1 = cputime;

t1_2 = cputime;
out2 = DRSPmix_Equivalent(H,etas,alphas,MUS,SIGMAS,gam1,gam2);
t2_2 = cputime;

t1_3 = cputime;
out3 = DRSPmix_Equivalent_v2(H,etas,alphas,MUS,SIGMAS,gam1,gam2);
t2_3 = cputime;

t1_1 = cputime;
out4 = DRSPmix_Dual_1dim_v2(H,etas,alphas,MUS,SIGMAS,gam1,gam2);
t2_1 = cputime;

t1_5 = cputime;
out5 = DRSPmix_Dual(H,etas,alphas,MUS,SIGMAS,gam1,gam2);
t2_5 = cputime;

t1_6 = cputime;
out6 = DRSPmix_Dual_v2(H,etas,alphas,MUS,SIGMAS,gam1,gam2);
t2_6 = cputime;

out2 = [out2{1},out2{2}];
out3 = [out3{1},out3{2}];
out4 = [out4{1},out4{2}];
out5 = [out5{1},out5{2}];

times = [(t2_1-t1_1),(t2_2-t1_2),(t2_3-t1_3),(t2_4-t1_4),(t2_5-t1_5)]./60;

clear MUS alphas SIGMAS etas H A alpha k n N K z
clear t1_1 t1_2 t1_3 t1_4 t1_5 t2_1 t2_2 t2_3 t2_4 t2_5