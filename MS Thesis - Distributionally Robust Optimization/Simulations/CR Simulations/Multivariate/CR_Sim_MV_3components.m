alphas  = [1/3,1/3,1/3]; mus = [1.7,1.8;0,-0.5;-1.7,1.8]; taus  = sqrt([1,1;1,1;1,1]); [K,d] = size(mus);
r0      = chi2inv(0.6,d); r = [r0,r0,r0]; 
sigs    = Comp_True_Sigmas(mus,taus,r,d);
nu      = 15; del = 0.1; N = 6;
M       = Compute_M_v2(alphas,mus,taus,r,del,d); 

mus = [1.7,1.8;0,-0.5;-1.7,1.8];
SIM_MV_3C_0_percent = Simulation_CRtest_MV(200,M,alphas,mus,taus,r,del,nu);
save('SIM_MV_3C_0_percent.mat','SIM_MV_3C_0_percent');

mus = [1.37,1.6;0,-0.5;-1.37,1.6];
SIM_MV_3C_1_percent = Simulation_CRtest_MV(200,M,alphas,mus,taus,r,del,nu);
save('SIM_MV_3C_1_percent.mat','SIM_MV_3C_1_percent');

mus = [1.27,1.57;0,-0.5;-1.27,1.57];
SIM_MV_3C_2_percent = Simulation_CRtest_MV(200,M,alphas,mus,taus,r,del,nu);
save('SIM_MV_3C_2_percent.mat','SIM_MV_3C_2_percent');

mus = [1.24,1.52;0,-0.5;-1.24,1.52];
SIM_MV_3C_3_percent = Simulation_CRtest_MV(200,M,alphas,mus,taus,r,del,nu);
save('SIM_MV_3C_3_percent.mat','SIM_MV_3C_3_percent');

mus = [1.17,1.51;0,-0.5;-1.17,1.51];
SIM_MV_3C_4_percent = Simulation_CRtest_MV(200,M,alphas,mus,taus,r,del,nu);
save('SIM_MV_3C_4_percent.mat','SIM_MV_3C_4_percent');

mus = [1.123,1.5;0,-0.5;-1.123,1.5];
SIM_MV_3C_5_percent = Simulation_CRtest_MV(200,M,alphas,mus,taus,r,del,nu);
save('SIM_MV_3C_5_percent.mat','SIM_MV_3C_5_percent');
