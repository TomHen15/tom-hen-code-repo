alphas  = [1/4,1/4,1/4,1/4]; taus  = sqrt([1,1;1,1;1,1;1,1]);
mus     = [1.37,1.36;1.35,-1.36;-1.35,1.36;-1.37,-1.36]; [K,d] = size(mus);
r0      = chi2inv(0.60,d); r = repmat(r0,1,K);
sigs    = Comp_True_Sigmas(mus,taus,r,d);
nu      = 15; del = 0.1; del_bar = 1-sqrt(1-del);
M       = Compute_M_v2(alphas,mus,taus,r,del,d); 

mus     = [1.37,1.36;1.35,-1.36;-1.35,1.36;-1.37,-1.36];
SIM_MV_4C_0_percent = Simulation_CRtest_MV(200,M,alphas,mus,taus,r,del,nu);
save('SIM_MV_4C_0_percent.mat','SIM_MV_4C_0_percent');

mus     = [1.3,1.26;1.28,-1.26;-1.28,1.26;-1.3,-1.26];
SIM_MV_4C_1_percent = Simulation_CRtest_MV(200,M,alphas,mus,taus,r,del,nu);
save('SIM_MV_4C_1_percent.mat','SIM_MV_4C_1_percent');

mus     = [1.25,1.23;1.23,-1.23;-1.23,1.23;-1.25,-1.23];
SIM_MV_4C_2_percent = Simulation_CRtest_MV(200,M,alphas,mus,taus,r,del,nu);
save('SIM_MV_4C_2_percent.mat','SIM_MV_4C_2_percent');

mus     = [1.22,1.19;1.20,-1.19;-1.20,1.19;-1.22,-1.19];
SIM_MV_4C_3_percent = Simulation_CRtest_MV(200,M,alphas,mus,taus,r,del,nu);
save('SIM_MV_4C_3_percent.mat','SIM_MV_4C_3_percent');

mus     = [1.2,1.15;1.18,-1.15;-1.18,1.15;-1.2,-1.15];
SIM_MV_4C_4_percent = Simulation_CRtest_MV(40,M,alphas,mus,taus,r,del,nu);
save('SIM_MV_4C_4_percent.mat','SIM_MV_4C_4_percent');

mus     = [1.15,1.14;1.13,-1.14;-1.13,1.14;-1.15,-1.14];
SIM_MV_4C_5_percent = Simulation_CRtest_MV(200,M,alphas,mus,taus,r,del,nu);
save('SIM_MV_4C_5_percent.mat','SIM_MV_4C_5_percent');
