function out = Simulation_CRtest(N,M,alphas,mus,taus,r,del,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computes the Estimators and does CR test with various methods of estimation
% The methods are
%   - Use underlying components
%   - Standard GMM EM
%   - Kmeans++ Alone
%   - Kmeans++ followed by optimization of LL for taus.
%   - EM for TNM with known truncation radii
%   - EM for TNM with estimated truncation radii (first version)
% Input: 
%   N       = number of simulations
%   M       = sample size
%   alphas  = mixture weights
%   mus     = mu parameters of mixture components
%   taus    = tau parameters of mixture components
%   r       = truncation radii of mixture components
%   nu      = smoothing parameter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[~,K]                   = size(alphas);
Z0 = zeros(N,K);   Z1   = zeros(N,1);
del_bar                 = 1 - sqrt(1-del);
    
%%%% compute true parameter values of truncated normals %%%%
sigs                    = Comp_True_Sigmas(mus,taus,r,1); 
M1                      = Compute_M(alphas,mus,taus,r,del,1);                       %used in DRSP parameter computation

%%%% Store all the relevant parameters for output %%%%
params = table(alphas,mus,taus,sigs,r,M);                                           %save parameters
params.Properties.VariableNames = {'alphas','mus','taus','sigs','truncation_radii','M'};

%%%% define data containers %%%% 
Estimators_nonEM = table(Z0,Z0,Z0);
Estimators_nonEM.Properties.VariableNames = {'alphas','mus','sigs'};
CR_Data_nonEM = table(Z0,Z0,Z0,Z0,Z0,Z0,Z1,Z1,Z1);                                  %this stores all the CR test 
CR_Data_nonEM.Properties.VariableNames = {'mus','mus_hat','mus_res','sigs','sigs_hat','sigs_res','mus_res_all','sigs_res_all','CR_res'};
CR_SigData_nonEM = table(Z0,Z0,Z0,Z0,Z0);                                           %this stores the data about the variance test
CR_SigData_nonEM.Properties.VariableNames = {'lower_bounds','sigs_squared','upper_bounds','lower_res','upper_res'};

Estimators_GMM_EM = table(Z0,Z0,Z0,Z1);
Estimators_GMM_EM.Properties.VariableNames = {'alphas','mus','sigs','MCR'};
CR_Data_GMM_EM = table(Z0,Z0,Z0,Z0,Z0,Z0,Z1,Z1,Z1);                                 %this stores all the CR test 
CR_Data_GMM_EM.Properties.VariableNames = {'mus','mus_hat','mus_res','sigs','sigs_hat','sigs_res','mus_res_all','sigs_res_all','CR_res'};
CR_SigData_GMM_EM = table(Z0,Z0,Z0,Z0,Z0);                                          %this stores the data about the variance test
CR_SigData_GMM_EM.Properties.VariableNames = {'lower_bounds','sigs_squared','upper_bounds','lower_res','upper_res'};

Estimators_KM = table(Z0,Z0,Z0,Z1);
Estimators_KM.Properties.VariableNames = {'alphas','mus','sigs','MCR'};
CR_Data_KM = table(Z0,Z0,Z0,Z0,Z0,Z0,Z1,Z1,Z1);                                     %this stores all the CR test 
CR_Data_KM.Properties.VariableNames = {'mus','mus_hat','mus_res','sigs','sigs_hat','sigs_res','mus_res_all','sigs_res_all','CR_res'};
CR_SigData_KM = table(Z0,Z0,Z0,Z0,Z0);                                              %this stores the data about the variance test
CR_SigData_KM.Properties.VariableNames = {'lower_bounds','sigs_squared','upper_bounds','lower_res','upper_res'};

Estimators_KM_LL = table(Z0,Z0,Z0,Z1);
Estimators_KM_LL.Properties.VariableNames = {'alphas','mus','sigs','MCR'};
CR_Data_KM_LL = table(Z0,Z0,Z0,Z0,Z0,Z0,Z1,Z1,Z1);                                  %this stores all the CR test 
CR_Data_KM_LL.Properties.VariableNames = {'mus','mus_hat','mus_res','sigs','sigs_hat','sigs_res','mus_res_all','sigs_res_all','CR_res'};
CR_SigData_KM_LL = table(Z0,Z0,Z0,Z0,Z0);                                           %this stores the data about the variance test
CR_SigData_KM_LL.Properties.VariableNames = {'lower_bounds','sigs_squared','upper_bounds','lower_res','upper_res'};

Estimators_TNM_EM = table(Z0,Z0,Z0,Z1,Z1);
Estimators_TNM_EM.Properties.VariableNames = {'alphas','mus','sigs','MCR','iters'};
CR_Data_TNM_EM = table(Z0,Z0,Z0,Z0,Z0,Z0,Z1,Z1,Z1);                                 %this stores all the CR test 
CR_Data_TNM_EM.Properties.VariableNames = {'mus','mus_hat','mus_res','sigs','sigs_hat','sigs_res','mus_res_all','sigs_res_all','CR_res'};
CR_SigData_TNM_EM = table(Z0,Z0,Z0,Z0,Z0);                                          %this stores the data about the variance test
CR_SigData_TNM_EM.Properties.VariableNames = {'lower_bounds','sigs_squared','upper_bounds','lower_res','upper_res'};

Estimators_TNM_EM_EstRadii = table(Z0,Z0,Z0,Z0,Z1,Z1);
Estimators_TNM_EM_EstRadii.Properties.VariableNames = {'alphas','mus','sigs','radii','MCR','iters'};
CR_Data_TNM_EM_EstRadii = table(Z0,Z0,Z0,Z0,Z0,Z0,Z1,Z1,Z1);                        %this stores all the CR test 
CR_Data_TNM_EM_EstRadii.Properties.VariableNames = {'mus','mus_hat','mus_res','sigs','sigs_hat','sigs_res','mus_res_all','sigs_res_all','CR_res'};
CR_SigData_TNM_EM_EstRadii = table(Z0,Z0,Z0,Z0,Z0);                                 %this stores the data about the variance test
CR_SigData_TNM_EM_EstRadii.Properties.VariableNames = {'lower_bounds','sigs_squared','upper_bounds','lower_res','upper_res'};

%%%% perform simulation %%%%
parfor_progress(N);
parfor i = 1:N 
    %%%% generate the sample %%%%
    samp                             = Sample_TNM(M,alphas,mus,taus,r);     
    xi                               = samp.mix;
    Y                                = samp.labels;
    
    %%% Estimate with underlying components %%%%
    mus_hat                          = mean(samp.comps);
    sigs_hat                         = std(samp.comps,1);
    etas                             = num2cell(samp.comps,1);
    Estimators_nonEM(i,:)            = {alphas,mus_hat,sigs_hat};
    [alpha_bar,beta_bar,~,~]         = Comp_DRSP_Parameters(etas,mus_hat,sigs_hat,M1,del_bar);
    CR_res                           = Test_CR(mus,sigs,mus_hat,sigs_hat,alpha_bar,beta_bar); 
    mus_res_comp                     = CR_res.mu_res;                                             
    sigs_res_comp                    = CR_res.sig_res;                                           
    mus_res_all                      = prod(mus_res_comp);                                        
    sigs_res_all                     = prod(sigs_res_comp);                                      
    CR_Data_nonEM(i,:)               = {mus,mus_hat,mus_res_comp,...
                                        sigs,sigs_hat,sigs_res_comp,...
                                        mus_res_all,sigs_res_all,CR_res.CR_res};                  
    CR_SigData_nonEM(i,:)            = {CR_res.sig_lower_bound,sigs.^2,CR_res.sig_upper_bound,...
                                        CR_res.sig_res_lower,CR_res.sig_res_upper};              
      
    %%% Estimate with GMM EM %%%%
    out_GMM_EM                       = GMM_EM(xi,K);
    MCR                              = 100*mean(Y~=out_GMM_EM.labels);
    Estimators_GMM_EM(i,:)           = {out_GMM_EM.alphas,out_GMM_EM.mus,out_GMM_EM.sigs,MCR};
    mus_hat                          = out_GMM_EM.mus;
    sigs_hat                         = out_GMM_EM.sigs;
    Y_hat                            = out_GMM_EM.labels;
    etas = cell(1,K);
    for k=1:K
        ind = Y_hat == k; 
        etas{k} = xi(ind);
    end
    [alpha_bar,beta_bar,~,~]         = Comp_DRSP_Parameters(etas,mus_hat,sigs_hat,M1,del_bar);
    CR_res                           = Test_CR(mus,sigs,mus_hat,sigs_hat,alpha_bar,beta_bar); 
    mus_res_comp                     = CR_res.mu_res;                                             
    sigs_res_comp                    = CR_res.sig_res;                                           
    mus_res_all                      = prod(mus_res_comp);                                        
    sigs_res_all                     = prod(sigs_res_comp);                                      
    CR_Data_GMM_EM(i,:)              = {mus,mus_hat,mus_res_comp,...
                                        sigs,sigs_hat,sigs_res_comp,...
                                        mus_res_all,sigs_res_all,CR_res.CR_res};                  
    CR_SigData_GMM_EM(i,:)           = {CR_res.sig_lower_bound,sigs.^2,CR_res.sig_upper_bound,...
                                        CR_res.sig_res_lower,CR_res.sig_res_upper};  
    
    %%% Estimate with KMeans++ %%%%
    out_KM                           = KmeansPP(xi,K,15);
    MCR                              = 100*mean(Y~=out_KM.labels);
    Estimators_KM(i,:)               = {out_KM.alphas,out_KM.mus,out_KM.sigs,MCR};
    mus_hat                          = out_KM.mus;
    sigs_hat                         = out_KM.sigs;
    Y_hat                            = out_KM.labels;
    etas = cell(1,K);
    for k=1:K
        ind = Y_hat == k; 
        etas{k} = xi(ind);
    end
    [alpha_bar,beta_bar,~,~]         = Comp_DRSP_Parameters(etas,mus_hat,sigs_hat,M1,del_bar);
    CR_res                           = Test_CR(mus,sigs,mus_hat,sigs_hat,alpha_bar,beta_bar); 
    mus_res_comp                     = CR_res.mu_res;                                             
    sigs_res_comp                    = CR_res.sig_res;                                           
    mus_res_all                      = prod(mus_res_comp);                                        
    sigs_res_all                     = prod(sigs_res_comp);                                      
    CR_Data_KM(i,:)                  = {mus,mus_hat,mus_res_comp,...
                                        sigs,sigs_hat,sigs_res_comp,...
                                        mus_res_all,sigs_res_all,CR_res.CR_res};                  
    CR_SigData_KM(i,:)               = {CR_res.sig_lower_bound,sigs.^2,CR_res.sig_upper_bound,...
                                        CR_res.sig_res_lower,CR_res.sig_res_upper};  
    
    %%% Estimate KMeans++ and LL Optimization for sigmas %%%%
    out_KM_LL                        = OptLL_Sigmas(samp,out_KM,r,nu);
    Estimators_KM_LL(i,:)            = {out_KM_LL.alphas,out_KM_LL.mus,out_KM_LL.sigs,out_KM_LL.MCR};
    mus_hat                          = out_KM_LL.mus;
    sigs_hat                         = out_KM_LL.sigs;
    Y_hat                            = out_KM_LL.labels;
    etas = cell(1,K);
    for k=1:K
        ind = Y_hat == k; 
        etas{k} = xi(ind);
    end
    [alpha_bar,beta_bar,~,~]         = Comp_DRSP_Parameters(etas,mus_hat,sigs_hat,M1,del_bar);
    CR_res                           = Test_CR(mus,sigs,mus_hat,sigs_hat,alpha_bar,beta_bar); 
    mus_res_comp                     = CR_res.mu_res;                                             
    sigs_res_comp                    = CR_res.sig_res;                                           
    mus_res_all                      = prod(mus_res_comp);                                        
    sigs_res_all                     = prod(sigs_res_comp);                                      
    CR_Data_KM_LL(i,:)               = {mus,mus_hat,mus_res_comp,...
                                        sigs,sigs_hat,sigs_res_comp,...
                                        mus_res_all,sigs_res_all,CR_res.CR_res};                  
    CR_SigData_KM_LL(i,:)            = {CR_res.sig_lower_bound,sigs.^2,CR_res.sig_upper_bound,...
                                        CR_res.sig_res_lower,CR_res.sig_res_upper};    
    
    %%% Estimate with TNM EM %%%%
    out_TNM_EM                       = TNM_EM(xi,K,r,nu);
    MCR                              = 100*mean(Y~=out_TNM_EM.labels);
    Estimators_TNM_EM(i,:)           = {out_TNM_EM.alphas,out_TNM_EM.mus,out_TNM_EM.sigs,MCR,out_TNM_EM.iter};
    mus_hat                          = out_TNM_EM.mus;
    sigs_hat                         = out_TNM_EM.sigs;
    Y_hat                            = out_TNM_EM.labels;
    etas = cell(1,K);
    for k=1:K
        ind = Y_hat == k; 
        etas{k} = xi(ind);
    end
    [alpha_bar,beta_bar,~,~]         = Comp_DRSP_Parameters(etas,mus_hat,sigs_hat,M1,del_bar);
    CR_res                           = Test_CR(mus,sigs,mus_hat,sigs_hat,alpha_bar,beta_bar); 
    mus_res_comp                     = CR_res.mu_res;                                             
    sigs_res_comp                    = CR_res.sig_res;                                           
    mus_res_all                      = prod(mus_res_comp);                                        
    sigs_res_all                     = prod(sigs_res_comp);                                      
    CR_Data_TNM_EM(i,:)               = {mus,mus_hat,mus_res_comp,...
                                        sigs,sigs_hat,sigs_res_comp,...
                                        mus_res_all,sigs_res_all,CR_res.CR_res};                  
    CR_SigData_TNM_EM(i,:)            = {CR_res.sig_lower_bound,sigs.^2,CR_res.sig_upper_bound,...
                                        CR_res.sig_res_lower,CR_res.sig_res_upper};
    
    %%% Estimate with TNM EM using radii estimator %%%%
    out_TNM_EM_EstRadii              = TNM_EM_EstRadii(xi,K,nu,20);
    MCR                              = 100*mean(Y~=out_TNM_EM_EstRadii.labels);
    Estimators_TNM_EM_EstRadii(i,:)  = {out_TNM_EM_EstRadii.alphas,out_TNM_EM_EstRadii.mus,out_TNM_EM_EstRadii.sigs,out_TNM_EM_EstRadii.radii,MCR,out_TNM_EM_EstRadii.iter};
    mus_hat                          = out_TNM_EM_EstRadii.mus;
    sigs_hat                         = out_TNM_EM_EstRadii.sigs;
    Y_hat                            = out_TNM_EM_EstRadii.labels;
    etas = cell(1,K);
    for k=1:K
        ind = Y_hat == k; 
        etas{k} = xi(ind);
    end
    [alpha_bar,beta_bar,~,~]         = Comp_DRSP_Parameters(etas,mus_hat,sigs_hat,M1,del_bar);
    CR_res                           = Test_CR(mus,sigs,mus_hat,sigs_hat,alpha_bar,beta_bar); 
    mus_res_comp                     = CR_res.mu_res;                                             
    sigs_res_comp                    = CR_res.sig_res;                                           
    mus_res_all                      = prod(mus_res_comp);                                        
    sigs_res_all                     = prod(sigs_res_comp);                                      
    CR_Data_TNM_EM_EstRadii(i,:)     = {mus,mus_hat,mus_res_comp,...
                                        sigs,sigs_hat,sigs_res_comp,...
                                        mus_res_all,sigs_res_all,CR_res.CR_res};                  
    CR_SigData_TNM_EM_EstRadii(i,:)  = {CR_res.sig_lower_bound,sigs.^2,CR_res.sig_upper_bound,...
                                        CR_res.sig_res_lower,CR_res.sig_res_upper};
                                    
    parfor_progress; 
end
parfor_progress(0);   

%%%% Organize the output %%%%
out                                  = struct();
out.params                           = params;

% save all the estimator data of the all methods
out.Estimators.NonEM                 = Estimators_nonEM;
out.Estimators.GMM_EM                = Estimators_GMM_EM;
out.Estimators.KM                    = Estimators_KM;
out.Estimators.MM_LL                 = Estimators_KM_LL;
out.Estimators.TNM_EM                = Estimators_TNM_EM;
out.Estimators.TNM_EM_EstRadii       = Estimators_TNM_EM_EstRadii;

% save summary of estimation results of the different methods
temp                                 = {alphas,mus,sigs,0;...
                                        mean(Estimators_nonEM.alphas),mean(Estimators_nonEM.mus),mean(Estimators_nonEM.sigs),0;...
                                        mean(Estimators_GMM_EM.alphas),mean(Estimators_GMM_EM.mus),mean(Estimators_GMM_EM.sigs),mean(Estimators_GMM_EM.MCR);...
                                        mean(Estimators_KM.alphas),mean(Estimators_KM.mus),mean(Estimators_KM.sigs),mean(Estimators_KM.MCR);...
                                        mean(Estimators_KM_LL.alphas),mean(Estimators_KM_LL.mus),mean(Estimators_KM_LL.sigs),mean(Estimators_KM_LL.MCR);...
                                        mean(Estimators_TNM_EM.alphas),mean(Estimators_TNM_EM.mus),mean(Estimators_TNM_EM.sigs),mean(Estimators_TNM_EM.MCR);...                                        
                                        mean(Estimators_TNM_EM_EstRadii.alphas),mean(Estimators_TNM_EM_EstRadii.mus),mean(Estimators_TNM_EM_EstRadii.sigs),mean(Estimators_TNM_EM_EstRadii.MCR)};
out.Est_Stats                        = cell2table(temp);
out.Est_Stats.Properties.VariableNames   = {'mean_alphas','mean_mus','mean_sigs','mean_MCR'};
out.Est_Stats.Properties.RowNames        = {'True_Values','Non_EM','GMM_EM','KM','KM_LL','TNM_EM','TNM_EM_EstRadii'};

% save summary of mean squared errors of the different methods
temp                                 = {mean((Estimators_nonEM.alphas - alphas).^2),mean((Estimators_nonEM.mus - mus).^2),mean((Estimators_nonEM.sigs - sigs).^2);
                                        mean((Estimators_GMM_EM.alphas - alphas).^2),mean((Estimators_GMM_EM.mus - mus).^2),mean((Estimators_GMM_EM.sigs - sigs).^2);...
                                        mean((Estimators_KM.alphas - alphas).^2),mean((Estimators_KM.mus - mus).^2),mean((Estimators_KM.sigs - sigs).^2);...
                                        mean((Estimators_KM_LL.alphas - alphas).^2),mean((Estimators_KM_LL.mus - mus).^2),mean((Estimators_KM_LL.sigs - sigs).^2);...
                                        mean((Estimators_TNM_EM.alphas - alphas).^2),mean((Estimators_TNM_EM.mus - mus).^2),mean((Estimators_TNM_EM.sigs - sigs).^2);...
                                        mean((Estimators_TNM_EM_EstRadii.alphas - alphas).^2),mean((Estimators_TNM_EM_EstRadii.mus - mus).^2),mean((Estimators_TNM_EM_EstRadii.sigs - sigs).^2)}; 

out.Est_Err                          = cell2table(temp);
out.Est_Err.Properties.VariableNames = {'alphas_mean_squared_err','mus_mean_squared_err','sigs_mean_squared_err'};
out.Est_Err.Properties.RowNames      = {'Non_EM','GMM_EM','KM','KM_LL','TNM_EM','TNM_EM_EstRadii'};
 
out.MeanMCR                          = table(mean(Estimators_GMM_EM.MCR),mean(Estimators_KM.MCR),...
                                        mean(Estimators_KM_LL.MCR),mean(Estimators_TNM_EM.MCR),mean(Estimators_TNM_EM_EstRadii.MCR));
out.MeanMCR.Properties.VariableNames = {'GMM_EM','KM','KM_LL','TNM_EM','TNM_EM_EstRadii'};

% Save the raw CR results of all the methods
out.CR_Res.nonEM                    = Simulation_CR_organizeRes(sigs,CR_Data_nonEM,CR_SigData_nonEM);
out.CR_Res.GMM_EM                   = Simulation_CR_organizeRes(sigs,CR_Data_GMM_EM,CR_SigData_GMM_EM);
out.CR_Res.KM                       = Simulation_CR_organizeRes(sigs,CR_Data_KM,CR_SigData_KM);
out.CR_Res.KM_LL                    = Simulation_CR_organizeRes(sigs,CR_Data_KM_LL,CR_SigData_KM_LL);
out.CR_Res.TNM_EM                   = Simulation_CR_organizeRes(sigs,CR_Data_TNM_EM,CR_SigData_TNM_EM);
out.CR_Res.TNM_EM_EstRadii          = Simulation_CR_organizeRes(sigs,CR_Data_TNM_EM_EstRadii,CR_SigData_TNM_EM_EstRadii);

% save summary of CR results of the different methods 
Z2 = zeros(6,K); Z3 = zeros(6,1);
CR_Stats                            = table(Z2,Z2,Z3,Z3,Z3,Z3);
CR_Stats.Properties.VariableNames   = {'mean_mus_hat','mean_sigs_hat','C1_prob','C2_prob','C3_prob','CR_prob'};
CR_Stats.Properties.RowNames        = {'Non_EM','GMM_EM','KM','KM_LL','TNM_EM','TNM_EM_EstRadii'};
CR_Stats(1,:)                       = {out.CR_Res.nonEM.CR_Stats.mean_mus_hat,out.CR_Res.nonEM.CR_Stats.mean_sigs_hat,...
                                      out.CR_Res.nonEM.CR_Stats.C1_prob,out.CR_Res.nonEM.CR_Stats.C2_prob,...
                                      out.CR_Res.nonEM.CR_Stats.C3_prob,out.CR_Res.nonEM.CR_Stats.Combined_prob};
CR_Stats(2,:)                       = {out.CR_Res.GMM_EM.CR_Stats.mean_mus_hat,out.CR_Res.GMM_EM.CR_Stats.mean_sigs_hat,...
                                      out.CR_Res.GMM_EM.CR_Stats.C1_prob,out.CR_Res.GMM_EM.CR_Stats.C2_prob,...
                                      out.CR_Res.GMM_EM.CR_Stats.C3_prob,out.CR_Res.GMM_EM.CR_Stats.Combined_prob};
CR_Stats(3,:)                       = {out.CR_Res.KM.CR_Stats.mean_mus_hat,out.CR_Res.KM.CR_Stats.mean_sigs_hat,...
                                      out.CR_Res.KM.CR_Stats.C1_prob,out.CR_Res.KM.CR_Stats.C2_prob,...
                                      out.CR_Res.KM.CR_Stats.C3_prob,out.CR_Res.KM.CR_Stats.Combined_prob};
CR_Stats(4,:)                       = {out.CR_Res.KM_LL.CR_Stats.mean_mus_hat,out.CR_Res.KM_LL.CR_Stats.mean_sigs_hat,...
                                      out.CR_Res.KM_LL.CR_Stats.C1_prob,out.CR_Res.KM_LL.CR_Stats.C2_prob,...
                                      out.CR_Res.KM_LL.CR_Stats.C3_prob,out.CR_Res.KM_LL.CR_Stats.Combined_prob};
CR_Stats(5,:)                       = {out.CR_Res.TNM_EM.CR_Stats.mean_mus_hat,out.CR_Res.TNM_EM.CR_Stats.mean_sigs_hat,...
                                      out.CR_Res.TNM_EM.CR_Stats.C1_prob,out.CR_Res.TNM_EM.CR_Stats.C2_prob,...
                                      out.CR_Res.TNM_EM.CR_Stats.C3_prob,out.CR_Res.TNM_EM.CR_Stats.Combined_prob};
CR_Stats(6,:)                       = {out.CR_Res.TNM_EM_EstRadii.CR_Stats.mean_mus_hat,out.CR_Res.TNM_EM_EstRadii.CR_Stats.mean_sigs_hat,...
                                      out.CR_Res.TNM_EM_EstRadii.CR_Stats.C1_prob,out.CR_Res.TNM_EM_EstRadii.CR_Stats.C2_prob,...
                                      out.CR_Res.TNM_EM_EstRadii.CR_Stats.C3_prob,out.CR_Res.TNM_EM_EstRadii.CR_Stats.Combined_prob};                                                                 
out.CR_Stats                        = CR_Stats;

% save summary of data about the variance part of the CR test
out.Sig_Stats.nonEM                 = out.CR_Res.nonEM.CR_SigStats;
out.Sig_Stats.GMM_EM                = out.CR_Res.GMM_EM.CR_SigStats;
out.Sig_Stats.KM                    = out.CR_Res.KM.CR_SigStats;
out.Sig_Stats.KM_LL                 = out.CR_Res.KM_LL.CR_SigStats;
out.Sig_Stats.TNM_EM                = out.CR_Res.TNM_EM.CR_SigStats;
out.Sig_Stats.TNM_EM_EstRadii       = out.CR_Res.TNM_EM_EstRadii.CR_SigStats;

end