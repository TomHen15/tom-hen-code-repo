%%% Simulations with standard Sampling - Three Clusters %%%
mus = [7,0,-7]; taus = [2.2,2.2,2.2]; alphas = [1/3,1/3,1/3]; del = 0.1; nu = 100;
r   = [3.6,3.6,3.6];
M   = Compute_M_v2(alphas,mus,taus,r,del,1);
H = @(x,y) abs(x-y); N = 200;

mus = [7.3,0,-7.3]; 
SIM_0_percent_3c = Simulation_Combo_EstRadii(N,M,H,alphas,mus,taus,r,del,nu);
save('SIM_0_percent_3c.mat','SIM_0_percent_3c');

mus = [6.925,0,-6.925]; 
SIM_1_percent_3c = Simulation_Combo_EstRadii(N,M,H,alphas,mus,taus,r,del,nu);
save('SIM_1_percent_3c.mat','SIM_1_percent_3c');

mus = [6.68,0,-6.68]; 
SIM_2_percent_3c = Simulation_Combo_EstRadii(N,M,H,alphas,mus,taus,r,del,nu);
save('SIM_2_percent_3c.mat','SIM_2_percent_3c');

mus = [6.45,0,-6.45];
SIM_3_percent_3c = Simulation_Combo_EstRadii(N,M,H,alphas,mus,taus,r,del,nu);
save('SIM_3_percent_3c.mat','SIM_3_percent_3c');

mus = [6.238,0,-6.238];
SIM_4_percent_3c = Simulation_Combo_EstRadii(N,M,H,alphas,mus,taus,r,del,nu);
save('SIM_4_percent_3c.mat','SIM_4_percent_3c');

mus = [6.05,0,-6.05]; 
SIM_5_percent_3c = Simulation_Combo_EstRadii(N,M,H,alphas,mus,taus,r,del,nu);
save('SIM_5_percent_3c.mat','SIM_5_percent_3c');