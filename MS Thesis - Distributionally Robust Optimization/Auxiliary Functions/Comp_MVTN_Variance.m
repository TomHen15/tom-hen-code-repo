function sig = Comp_MVTN_Variance(mu,tau,r)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computes the standard deviations of the truncated Normal mixture
%   mus     = vector of means of underlying normal components
%   tau     = vector of standard deviations of underlying normal components 
%   r       = truncation radius
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[~,d] = size(mu);
fun = @(z) 1/sqrt(2*pi)/chi2cdf(r,d) * z.^2 .* exp(-1/2*z.^2) .* chi2cdf(r-z.^2,d-1);
C   = integral(fun,-sqrt(r),sqrt(r));
sig = sqrt(C)*tau;
end
