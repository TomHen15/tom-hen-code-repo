function out = Sample_TNM(M,alphas,mus,taus,r)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate data from mixture of K univarate trcunated normals.
% Input:
%   M       = number of samples
%   K       = number of components
%   alpha   = mixture weights
%   mus     = means for underlying normals
%   taus    = standard deviation for underlying normals
%   r       = truncation radii for components
% Output:
%   cell array containing the following
%    - matrix of  mixture components vectors
%    - vector of mixure sample
%    - vector of component labels for the sample
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
d = 1; % for higher dimensional extension
[~,K] = size(alphas);
etas = zeros(d*M,K);
for k=1:K
    p = makedist('Normal','mu',mus(k),'sigma',taus(k));
    t = truncate(p,mus(k)-r(k),mus(k)+r(k));
    etas(:,((k-1)*d+1):k*d) = random(t,M,1);
end 
z = mnrnd(1,alphas,M);
[~,Y] = max(z,[],2);

mix = zeros(M,1);
for k=1:K
   mix = mix + z(:,k).*etas(:,((k-1)*d+1):k*d);
end
out = struct();
out.mix = mix;
out.comps = etas;
out.labels = Y;
end 
