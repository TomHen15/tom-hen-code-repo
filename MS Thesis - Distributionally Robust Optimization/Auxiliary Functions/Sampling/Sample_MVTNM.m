function out = Sample_MVTNM(M,alphas,mus,taus,r)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate data from mixture of K univarate trcunated normals.
% Input:
%   M       = number of samples
%   K       = number of components
%   alphas  = mixture weights
%   mus     = cell array of mu vector parameters of components
%   taus    = cell array of matrices of tau parameters of components
%   r       = vector of truncation parameters of components
% Output:
%   cell array containing the following
%    - cell array of  mixture components matrices
%    - matrix of mixure samples
%    - vector of component labels 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[~,d] = size(mus(1)); %dimension
[~,K] = size(alphas);
etas = cell(d);
for k=1:K
    etas{k} = Sample_MVTN(M,mus{k},taus{k},r(k));
end 
z = mnrnd(1,alphas,M);
[~,Y] = max(z,[],2);

mix = zeros(M,d);
for k=1:K
   mix = mix + z(:,k).*etas{k};
end
out = struct();
out.mix = mix;
out.comps = etas;
out.labels = Y;
end 
