function out = Test_CR_MV(mus,sigs,mus_hat,sigs_hat,alphas_bar,betas_bar)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tests whether the confidence region constraints are satisfied (MV version)
% Input:
%   mus         = True means of mixture components
%   sigs        = True standard deviations of mixture components
%   mus_hat     = Estimators for means of mixture components
%   sigs_hat    = Estimators for standard deviations of mixture components
%   alphas_bar  = alpha_bar parameters for bounds of each component
%   betas_bar   = beta_bar parameters for bounds of each component
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[K,d] = size(mus_hat);

lower_bound      = zeros(K,d);
upper_bound      = zeros(K,d);
mu_res           = zeros(1,K);
sig_res_lower    = zeros(1,K);
sig_res_upper    = zeros(1,K);
for k=1:K
    lower_coeff         = 1/(1+alphas_bar(k));  
    upper_coeff         = 1/(1-alphas_bar(k)-betas_bar(k));  
    z                   = (mus(k,:)-mus_hat(k,:))./sigs_hat(k,:);
    mu_res(k)           = dot(z,z) <= betas_bar(k);
    lower_bound(k,:)    = lower_coeff .* (sigs_hat(k,:)).^2;
    upper_bound(k,:)    = upper_coeff .* (sigs_hat(k,:)).^2;
    sig_res_lower(k)    = min(lower_bound(k,:) <= (sigs(k,:).^2));
    sig_res_upper(k)    = min(upper_bound(k,:) >= (sigs(k,:).^2));
end

CR_res = 0;
if (sum(mu_res) + sum(sig_res_lower) + sum(sig_res_upper)) == 3*K
    CR_res = 1;
end

out = struct();
out.CR_res          = CR_res;                         % indicates whether all the CR criteria are satisfied
out.mu_res          = mu_res;                         % indicates whether the mu bound is satisfied
out.sig_res_lower   = sig_res_lower;                  % indicates whether the lower sigma bound is satisfied 
out.sig_res_upper   = sig_res_upper;                  % indicates whether the upper sigma bound is satisfied 
out.sig_res         = sig_res_lower.*sig_res_upper;   % indicates whether both sigma bounds are satisfied
out.sig_lower_bound = lower_bound;                    % value of sigma lower bound
out.sig_upper_bound = upper_bound;                    % value of sigma upper bound

end
