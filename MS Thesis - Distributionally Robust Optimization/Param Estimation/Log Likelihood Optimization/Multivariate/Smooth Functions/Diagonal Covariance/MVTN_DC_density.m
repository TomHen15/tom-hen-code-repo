function out = MVTN_DC_density(mu,tau,r,xi,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluates the density of MV truncated normal supported on levelset with truncation parameters r.
% This uses continuous surrogate for the interval indicator
% Input:
%   mu    = vector of mu parameters of MVTN
%   tau   = vector of tau parameters of MVTN with diagonal covariances
%           the entries are the standard deviations of the underlying
%           normal components.
%   r     = truncation parameter of MVTN
%   xi    = points of evaluation
%   nu  = parameter for indicator smoothing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[~,d]               = size(mu); % dimension
cov                 = diag(tau.^2);
normal_dens         = mvnpdf(xi,mu,cov);
normalization_const = chi2cdf(r^2,d);
indicator           = LS_SmoothInd(mu,cov,r,xi,nu);
out                 = normal_dens./normalization_const.*indicator;
end