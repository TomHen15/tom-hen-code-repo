function out = MVTNM_NLL_ns(alphas,mus,taus,r,xi)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluates the negative log-likelihood of mixture of MV truncated normals
% Input:
%   alphas = mixture weights
%   mu     = cell array of mu vector parameters of components
%   tau    = cell array of matrices of tau parameters of components
%   r      = vector of truncation parameters of components
%   xi     = points of evaluation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
TNM_dens = MVTNM_density_ns(alphas,mus,taus,r,xi);
out = -sum(log(TNM_dens));
end