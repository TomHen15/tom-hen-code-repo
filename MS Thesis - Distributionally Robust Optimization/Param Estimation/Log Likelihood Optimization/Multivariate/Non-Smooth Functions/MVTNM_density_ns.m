function out = MVTNM_density_ns(alphas,mus,taus,r,xi)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluates the density of mixture of multivariate truncated normals
% Input:
%   alphas = mixture weights
%   mu     = cell array of mu vector parameters of components
%   tau    = cell array of matrices of tau parameters of components
%   r      = vector of truncation parameters of components
%   xi     = point of evaluation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[N,~] = size(xi);
[~,K]  = size(r);   
dens_values = zeros(N,K); % contains the densities of each component at the evaluation points

for k=1:K
    dens_values(:,k) = MVTN_density_ns(mus{k},taus{k},r(k),xi);
end
out = sum(alphas.*dens_values,2);
end 