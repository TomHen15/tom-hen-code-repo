function out = OptLL_Sigmas(samp,init,r,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute Estimators using Kmeans++ and LL optimization
% LL optimization is used only for sigma computation
% Input: 
%   samp    = sample from mixture
%   init    = initialization values from KMeans++
%   r       = truncation radii for mixture components
%   nu      = smoothing parameter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xi                  = samp.mix; 

% get initial estimators from Kmeans++
mus_hat             = init.mus;
sigs_init           = init.sigs;
alphas_hat          = init.alphas;
        
% Compute estimators with LL optimization
% initializaiton is done using the Kmeans++ estimators.
options             = optimoptions('fminunc','Algorithm','quasi-newton','FiniteDifferenceType','central',...
                       'MaxFunctionEvaluations',10000,'MaxIterations',10000,...
                       'display','off');
initvals            = sigs_init;
fun                 = @(arg) TNM_NLL(alphas_hat,mus_hat,arg,r,xi,nu);
[taus_hat,~]        = fminunc(fun,initvals,options);

% classify and sort the output and labels to fit the order of the true parameters 
assignments         = LL_Classify(xi,alphas_hat,mus_hat,taus_hat,r,nu);
labels              = zeros(length(xi),1);
mus_hat2            = mus_hat; 
mus_hat3            = mus_hat; 
taus_hat2           = taus_hat;
alphas_hat2         = alphas_hat;
for i=1:length(mus_hat)
    [~,i1]          = max(mus_hat2);
    mus_hat(i)      = mus_hat2(i1);
    taus_hat(i)     = taus_hat2(i1);
    alphas_hat(i)   = alphas_hat2(i1);
    l = find(mus_hat3 == mus_hat(i),1);
    labels(assignments==l) = i;
    taus_hat2(i1)   = [];
    mus_hat2(i1)    = [];
    alphas_hat2(i1) = [];
end

% compute and return result
sigs_hat            = Comp_True_Sigmas(mus_hat,taus_hat,r,1);  % get the estimators for sigmas 
out                 = struct();
out.alphas          = alphas_hat;
out.mus             = mus_hat;
out.taus            = taus_hat;
out.sigs            = sigs_hat;
out.labels          = labels;
out.MCR             = 100*mean(labels~=samp.labels);

end
