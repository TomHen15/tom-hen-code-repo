function out = TN_density_ns(mu,tau,r,x)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluates the density of truncated normal supported on [mu-r,mu+r]
% Input:
% mu  = mu parameter of TN
% tau = tau parameter of TN
% r   = truncation radius of TN
% x   = points of evaluation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dens       = normpdf(x,mu,tau);
norm_const = (normcdf(r/tau)-normcdf(-r/tau));
indicator  = (x<=mu+r)&(x>=mu-r);
out        = (dens./norm_const.*indicator)';
end

