function out = SmoothInd(mu,r,x,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Smoothed indicactor on interval [mu-r,mu+r]
% Input:
% mu = value for mu
% r  = value for r
% x  = value for x
% nu = parameter for indicator smoothing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
a = exp(-nu*(x-(mu-r)+1/sqrt(nu)));
b = exp(-nu*((mu+r)-x+1/sqrt(nu)));
out = 1./((1+a).*(1+b));
end 