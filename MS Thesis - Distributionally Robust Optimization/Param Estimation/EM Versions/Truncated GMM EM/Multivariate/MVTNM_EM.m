function out = MVTNM_EM(samp,init,K,r,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perform EM for the truncated normal mixture
% Input:
%   samp        = sample data from mixture (including labels)
%   init        = initialization values (obtained from KMeans++)
%   K           = number of components
%   r           = truncation radii for components 
%   nu          = smoothing parameter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xi           = samp.mix;  
mus_hat      = init.mus;
sigs_hat     = init.sigs;
alphas_hat   = init.alphas;
[~,d]        = size(init.mus); 

%%% compute constants that transform from sigs to taus.
C            = zeros(K,1);
for k=1:K
    fun      = @(z) 1/sqrt(2*pi)/chi2cdf(r(k),d) * z.^2 .* exp(-1/2*z.^2) .* chi2cdf(r(k)-z.^2,d-1);
    C(k)     = 1./sqrt(integral(fun,-sqrt(r(k)),sqrt(r(k))));
end 
taus_hat     = C.*sigs_hat;

labels       = MVTNM_DC_LLClassify(xi,alphas_hat,mus_hat,taus_hat,r,nu);
for k=1:K
    ind           = labels == k; 
    etas          = xi(ind==1,:);
    alphas_hat(k) = sum(ind)/length(xi);
    sigs_hat(k,:) = sqrt(diag(cov(etas)));
    taus_hat(k,:) = C(k).*sigs_hat(k,:);
end

%%% compute initial complete log-likelihood value and iterate EM
LLval        = -MVTNM_DC_NLL(alphas_hat,mus_hat,taus_hat,r,xi,nu);
LL_vals      = LLval; %save the LL values

stop = 1; iter = 1;

while (stop && iter<=50)
    %iter
    % Expectation step
    posteriors = MVTNM_Posterior(xi,alphas_hat,mus_hat,taus_hat,r,nu);

    % Maximimization step
    params     = MVTNM_Parameters(xi,posteriors,taus_hat,r,nu);
    alphas_hat = params.alphas;
    mus_hat    = params.mus;
    taus_hat   = params.taus;
    
    labels     = MVTNM_DC_LLClassify(xi,alphas_hat,mus_hat,taus_hat,r,nu);
    for k=1:K
        ind           = labels == k; 
        etas          = xi(ind,:);
        alphas_hat(k) = sum(ind)/length(xi);
        sigs_hat(k,:) = sqrt(diag(cov(etas)));
        taus_hat(k,:) = C(k).*sigs_hat(k,:);
    end
    
    % Evaluate log-likelihood
    LLval      = -MVTNM_DC_NLL(alphas_hat,mus_hat,taus_hat,r,xi,nu);
    diff       = abs(LLval-LL_vals(length(LL_vals)));
    %disp(diff)
    if diff < 10^-4
        stop   = 0;
    end
    LL_vals    = [LL_vals,LLval];
    iter       = iter+1;
end

posteriors          = MVTNM_Posterior(xi,alphas_hat,mus_hat,taus_hat,r,nu);
[~,assignments]     = max(posteriors,[],2); %cluster assignment by maximal posterior

% this sorts the output and labels to fit the order of the true parameters 
labels              = zeros(length(xi),1);
mus_hat2            = mus_hat; 
mus_hat3            = mus_hat; 
taus_hat2           = taus_hat;
alphas_hat2         = alphas_hat;
for i=1:K
    [~,i1]          = max(mus_hat2(:,1));
    mus_hat(i,:)    = mus_hat2(i1,:);
    taus_hat(i,:)   = taus_hat2(i1,:);
    alphas_hat(i)   = alphas_hat2(i1);
    l = find(mus_hat3(:,1) == mus_hat(i,1),1);
    labels(assignments==l) = i;
    taus_hat2(i1,:) = [];
    mus_hat2(i1,:)  = [];
    alphas_hat2(i1) = [];
end

% sort output and return
sigs_hat            = Comp_True_Sigmas(mus_hat,taus_hat,r,d);
out                 = struct();
out.alphas          = alphas_hat;
out.mus             = mus_hat;
out.taus            = taus_hat;
out.sigs            = sigs_hat;
out.labels          = labels;
out.LLvals          = LL_vals;
out.iter            = iter;
end