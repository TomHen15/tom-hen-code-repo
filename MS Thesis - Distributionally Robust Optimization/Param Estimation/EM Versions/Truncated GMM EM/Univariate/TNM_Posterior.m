function posteriors = TNM_Posterior(xi,alphas_hat,mus_hat,taus_hat,r,nu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Classifies the mixture sample to components based on the LL estimators;
% Input:
%   xi          = mixture sample
%   alphas_hat  = Estimators for mixture weights
%   mus_hat     = Estimators for mu parameters
%   taus_hat    = Estimators for tau parameters 
%   r           = truncation radii for components
%   nu          = Smoothing parameter for indicators
% Output:
%   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[M,~]           = size(xi);
[~,K]           = size(alphas_hat);
temp            = zeros(M,K); 

for k=1:K
    temp(:,k)   = alphas_hat(k).*TN_density(mus_hat(k),taus_hat(k),r(k),xi,nu);
end

TNM_dens_values = sum(temp,2);
posteriors      = temp./TNM_dens_values;
