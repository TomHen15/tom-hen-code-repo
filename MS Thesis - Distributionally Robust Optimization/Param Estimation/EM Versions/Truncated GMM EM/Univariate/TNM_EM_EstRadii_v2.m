function out = TNM_EM_EstRadii_v2(xi,K,nu,maxit)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perform EM for the truncated normal mixture with radii estimation
% Input:
%   xi          = mixture data (column vector)
%   K           = number of components
%   nu          = smoothing parameter
%   maxit       = maximal number of iterations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

options         = optimoptions('fmincon','FiniteDifferenceType','central',...
                       'MaxFunctionEvaluations',10000,'MaxIterations',10000,...
                       'display','off');

%%% Initialize parameters using Kmeans++
init            = KmeansPP(xi,K,15);
alphas_hat(1,:) = init.alphas;
mus_hat(1,:)    = init.mus;
taus_hat(1,:)   = init.sigs;
Y_hat           = init.labels;

%%% use Kmeans++ initialization to obtain first estimate for r
r_min(1,:)      = zeros(1,K);
etas            = cell(1,K);
for k=1:K
    ind = Y_hat == k; 
    etas{k} = xi(ind);
    r_min(k)    = max(abs(etas{k}-mus_hat(k)));
end
r_hat(1,:) = r_min;

% solution for case k==2 where no iteration is required.
if K==2
    i                     = 2;
    init                  = TNM_EM(xi,K,r_hat,nu);
    alphas_hat(i,:)       = init.alphas;
    mus_hat(i,:)          = init.mus;
    taus_hat(i,:)         = init.taus;
    Y_hat                 = init.labels;
end

% solution for case k>2 where iteration is required.
if K>2 
    i=1;  
    % optimize w.r.t to r using the estiamtes for the other parameters
    initvals              = r_min(1,2:(K-1));
    fun                   = @(arg) TNM_NLL(alphas_hat,mus_hat,taus_hat,[r_min(1),arg,r_min(K)],xi,nu);
    lb                    = r_min(1,2:(K-1));
    ub                    = r_min(1,2:(K-1))+0.2*taus_hat(2:(K-1));
    [r_hat(i,2:(K-1)),~]  = fmincon(fun,initvals,[],[],[],[],lb,ub,[],options);
    
    %%% compute initial complete log-likelihood value and iterate EM
    LLval                 = -TNM_NLL(alphas_hat(i,:),mus_hat(i,:),taus_hat(i,:),r_hat(i,:),xi,nu);
    LL_vals               = LLval; %save the LL values
    
    % start iterative process for successive optimization
    stop = 1; 
    while (stop && i <= maxit)   
        % Expectation step
        posteriors            = TNM_Posterior(xi,alphas_hat(i,:),mus_hat(i,:),taus_hat(i,:),r_hat(i,:),nu);

        % Maximimization step
        params                = TNM_Parameters(xi,posteriors,taus_hat(i,:),r_hat(i,:),nu);
        i                     = i+1;  
        alphas_hat(i,:)       = params.alphas;
        mus_hat(i,:)          = params.mus;
        taus_hat(i,:)         = params.taus;
        sigs_hat(i,:)         = Comp_True_Sigmas(mus_hat(i,:),taus_hat(i,:),r_hat(i-1,:),1);
        
        % optimize for r with the new updates for the other parameters
        posteriors            = TNM_Posterior(xi,alphas_hat(i,:),mus_hat(i,:),taus_hat(i,:),r_hat(i-1,:),nu);
        [~,Y_hat]             = max(posteriors,[],2);
        etas = cell(K,1);
        for k=1:K
            ind = Y_hat == k; 
            etas{k}  = xi(ind);
            r_min(i,k) = max(abs(etas{k}-mus_hat(i,k)));
        end
        r_hat(i,:)            = r_min(i,:);
        initvals              = r_min(i,2:(K-1));
        lb                    = r_min(i,2:(K-1));
        ub                    = r_min(i,2:(K-1))+0.2*sigs_hat(i,2:(K-1));  %+0.2*taus_hat(i,2:(K-1));
        fun                   = @(arg) TNM_NLL(alphas_hat(i,:),mus_hat(i,:),taus_hat(i,:),[r_min(i,1),arg,r_min(i,K)],xi,nu);
        [r_hat(i,2:(K-1)),~]  = fmincon(fun,initvals,[],[],[],[],lb,ub,[],options);   
       
        % Evaluate log-likelihood
        LLval      = -TNM_NLL(alphas_hat(i,:),mus_hat(i,:),taus_hat(i,:),r_hat(i,:),xi,nu);
        diff(i)    = abs(LLval-LL_vals(length(LL_vals)));
        if diff(i) < 10^-4
            stop   = 0;
        end
        LL_vals    = [LL_vals,LLval];
    end
end

sigs_hat(i,:)       = Comp_True_Sigmas(mus_hat(i,:),taus_hat(i,:),r_hat(i,:),1);
out                 = struct();
out.LLdiff          = diff';
out.log.mus         = mus_hat;
out.log.taus        = taus_hat;
out.log.sigs        = sigs_hat;
out.log.radii       = r_hat;
out.log.rmin        = r_min;
out.alphas          = alphas_hat(i,:);
out.mus             = mus_hat(i,:);
out.taus            = taus_hat(i,:);
out.radii           = r_hat(i,:);
out.sigs            = sigs_hat(i,:);
out.labels          = Y_hat;
out.iter            = i;
end
    