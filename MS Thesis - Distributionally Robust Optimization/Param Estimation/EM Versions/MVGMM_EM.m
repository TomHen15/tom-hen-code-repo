function out = MVGMM_EM(xi,K)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Input:
%   xi          = mixture data (column vector)
%   K           = number of components
% Output:
%   alphas      = mixture weights
%   mus         = mixture means
%   sigmas      = mixture standard deviations
%   labels      = cluster labels
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[N,d] = size(xi);
[initMeans, assignments] = vl_kmeans(xi', K, ...
    'Algorithm','Lloyd', ...
    'MaxNumIterations',10);

initVars = zeros(d,K);
initPriors = zeros(1,K);

for i=1:K
    data_k = xi(assignments==i,:);
    initPriors(i) = size(data_k,1) / N;
    if size(data_k,1) == 0 || size(data_k,2) == 0
        initVars(:,i) = var(xi);
    else
        initVars(:,i) = var(data_k);
    end
end

[mus_hat,sigs_hat,alphas_hat,~,posteriors] = vl_gmm(xi', K, ...
    'initialization','custom', ...
    'InitMeans',initMeans, ...
    'InitCovariances',initVars, ...
    'InitPriors',initPriors);

[~,assignments] = max(posteriors',[],2); %EM labels
alphas_hat          = alphas_hat';
mus_hat             = mus_hat';
sigs_hat            = sigs_hat';

% this sorts the output and labels to fit the order of the true parameters 
labels              = zeros(length(xi),1);
mus_hat2            = mus_hat; 
mus_hat3            = mus_hat;
sigs_hat2           = sigs_hat;
alphas_hat2         = alphas_hat;
for i=1:K
    [~,i1]          = max(mus_hat2(:,1));
    mus_hat(i,:)    = mus_hat2(i1,:);
    sigs_hat(i,:)   = sigs_hat2(i1,:);
    alphas_hat(i)   = alphas_hat2(i1);
    l = find(mus_hat3(:,1) == mus_hat(i,1),1);
    labels(assignments==l) = i;
    sigs_hat2(i1,:) = [];
    mus_hat2(i1,:)  = [];
    alphas_hat2(i1) = [];
end

% sort output and return
out                 = struct();
out.alphas          = alphas_hat;
out.mus             = mus_hat;
out.sigs            = sigs_hat;
out.labels          = labels;
end 