function out = GMM_EM(xi,K)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Input:
%   xi          = mixture data (column vector)
%   K           = number of components
% Output:
%   alphas      = mixture weights
%   mus         = mixture means
%   sigmas      = mixture standard deviations
%   labels      = cluster labels
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[N,~] = size(xi);
sigs = zeros(1,K);
mus = zeros(1,K);
alphas = zeros(1,K);

[initMeans, assignments] = vl_kmeans(xi', K, ...
    'Algorithm','Lloyd', ...
    'MaxNumIterations',10);

initVars = zeros(1,K);
initPriors = zeros(1,K);

for i=1:K
    data_k = xi(assignments==i,:);
    initPriors(i) = size(data_k,2) / N;
    if size(data_k,1) == 0 || size(data_k,2) == 0
        initVars(:,i) = var(xi');
    else
        initVars(:,i) = var(data_k');
    end
end

[means,sigmas,priors,~,posteriors] = vl_gmm(xi', K, ...
    'initialization','custom', ...
    'InitMeans',initMeans, ...
    'InitCovariances',initVars, ...
    'InitPriors',initPriors);

[~,assignments] = max(posteriors',[],2); %EM labels

labels = zeros(N,1);
%this part sorts the label values
means2 = means;
stds = sqrt(sigmas);
%resort labels
for i=1:K
    [~,i1] = max(means2);
    mus(i) = means2(i1);
    sigs(i) = stds(i1);
    alphas(i) = priors(i1);
    l = find(means == mus(i),1);
    labels(assignments==l) = i;
    stds(i1) = [];
    means2(i1) = [];
    priors(i1) = [];
end

out             = struct();
out.mus         = mus;
out.sigs        = sigs;
out.alphas      = alphas;
out.labels      = labels;
end 