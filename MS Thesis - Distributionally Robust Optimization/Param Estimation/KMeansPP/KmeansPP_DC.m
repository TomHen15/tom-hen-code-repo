function out = KmeansPP_DC(xi,K,itr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Purpose: Compute K-means clustering with K clusters
% This version is suitable for the diagonal covariance MV case
% Input:
%   xi     = data points to be clustered
%   K      = desired number of cluters
%   itr    = number of iterations (deprecated)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[N,d]           = size(xi);
[assignments,~] = kmeans(xi,K);

% compute probabilities by cluster proportion
counts          = histc(assignments(:),unique(assignments));
priors          = (counts./N)';

sigs            = zeros(K,d);
mus             = zeros(K,d);
alphas          = zeros(1,K);

% compute means and standard-devs
mus_temp        = zeros(K,d);
sigs_temp       = zeros(K,d);
for k=1:K
    temp = xi(assignments==k,:);
    mus_temp(k,:)  = mean(temp);
    sigs_temp(k,:) = sqrt(diag(cov(temp)));
end

%this part sorts the label values
labels          = zeros(N,1);
mus2            = mus_temp;
for i=1:K
    [~,i1]          = max(mus2(:,1));
    mus(i,:)        = mus2(i1,:);
    sigs(i,:)       = sigs_temp(i1,:);
    alphas(i)       = priors(i1);
    l               = find(mus_temp(:,1) == mus(i,1),1);
    labels(assignments==l) = i;
    sigs_temp(i1,:) = [];
    mus2(i1,:)      = [];
    priors(i1)      = [];
end

out = struct();
out.mus = mus;
out.sigs = sigs;
out.alphas = alphas;
out.labels = labels;
if d == 1 
    out.mus = mus';
    out.sigs = sigs';
end
end