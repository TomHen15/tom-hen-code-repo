function C = KM_CompCentroids(xi,Y,K)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Purpose: Compute centroids for K-means iteration
% Input:
%   X = data points to be clustered
%   Y = current cluster assignments
%   K = number of clusters
% Output:
%   C = new centroids
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[~,n] = size(xi);
C = zeros(K,n);

for k=1:K
    ind = find(Y==k); %indices of rows of X corresponding to cluster k
    if not(isempty(ind))
        temp = xi(ind,:);  %the rows of X corresponding to the indices in ind
        C(k,:) = mean(temp,1);
    end
end
end 
    
    
